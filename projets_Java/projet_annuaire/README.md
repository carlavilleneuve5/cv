# Projet Annuaire

## Description 

Ce projet a été réalisé dans le cadre du troisième TP du cours de programmation Java à l'UQAM.
Il a pour but de créer la gestion d'un annuaire de contact.

## Fonctionnement

Il suffit de compiler et lancer la classe Test du projet de la facon suivante:

```sh
$ javac TestContact.java
$ java TestContact
```

## Dépendance 

* Compilateur Javac


