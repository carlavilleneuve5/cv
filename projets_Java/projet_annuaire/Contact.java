/**
 * Cette classe implémente l'objet Contact qui me permet de réunir différentes informations comme
 * le nom, le prénom, un ou plusieurs téléphones, une adresse, un ou plusieurs courriels, et de 
 * définir un contact en favori ou non.
 *  
 * @author Carla Villeneuve
 * Code permanent : VILC67270107
 * Courriel : cc191969@ens.uqam.ca
 * Cours : INF1120-30  
 * @version 2022-11-06 */

public class Contact{
    private final static String NOM_DEFAUT="Nom";
    private final static String PRENOM_DEFAUT="Prenom";

    private static int nbrContactsFavoris=0;

    private int nbrTelephone;
    private int nbrCourriels;

    private String nom;
    private String prenom;
    private Telephone [] telephones;
    private Adresse adresse;
    private String [] courriels;
    private boolean favori;
    
    public Contact(String nom, String prenom, Telephone tel, Adresse adresse, String courriel, boolean favori){
        this.nom=NOM_DEFAUT;
        this.prenom=PRENOM_DEFAUT;
        if (nom!=null && !nom.isEmpty()){
            this.nom=nom;
        }
        if (prenom!=null && !prenom.isEmpty()){
            this.prenom=prenom;
        }
        if (tel!=null){
            nbrTelephone=1;
            telephones = new Telephone[nbrTelephone];
            this.telephones[0]=tel;
        } else{
            nbrTelephone=0;
            telephones = new Telephone[nbrTelephone];
        }
        this.adresse=adresse;
        if (courriel!=null && !courriel.isEmpty()){
            nbrCourriels=1;
            courriels= new String[nbrCourriels];
            this.courriels[0]=courriel;
        } else{
            nbrCourriels=0;
            courriels  = new String[nbrCourriels];
        }
        this.favori=favori;
        if (favori){
            nbrContactsFavoris++;
        }
    }

    public Contact(String nom, String prenom, Adresse adresse){
        this(nom,prenom,null,adresse,null,false);
    }

    //-------------- Getters ------------------

    public String getNom(){
        return nom;
    }

    public String getPrenom(){
        return prenom;
    }

    public Adresse getAdresse(){
        return adresse;
    }

    public boolean isFavori(){
        return favori;
    }

  //-------------- Setters ------------------

  public void setNom(String nom){
    if (nom!=null && !nom.isEmpty()){
        this.nom=nom;
    }
  }

  public void setPrenom(String prenom){
    if (prenom!=null && !prenom.isEmpty()){
        this.prenom=prenom;
    }
  }

  public void setAdresse(Adresse adresse){
    this.adresse=adresse;
  }

  public void setFavori(boolean favori){
    if (!this.favori && favori){
        nbrContactsFavoris++;
        this.favori=favori;
    } else if (this.favori && !favori){
        nbrContactsFavoris--;
        this.favori=favori;
    }
  }

//-------------- Méthodes d'instance ------------------


    /** 
     * Redéfini la méthode toString afin d'afficher toutes les informations de l'objet Contact proprement.
     * @return une chaine de caractère contenant toutes les informations du Contact.
     */

    public String toString(){
        String favori="";
        String sTrim="";
        if (this.favori){
            favori="  [FAVORI]";
        }
        sTrim+=this.nom.toUpperCase()+", "+this.prenom+favori+"\n";
        sTrim+="\nTELEPHONE(S) :";
        if (this.telephones!=null && this.telephones.length!=0){
            for (Telephone t : this.telephones){
                sTrim+="\n"+t.toString();
            }   
        }
        sTrim+="\n\nADRESSE : ";
        if (this.adresse!=null){
            sTrim+="\n"+this.adresse.toString();
        }
        sTrim+="\n\nCOURRIEL(S) :";
        if (this.courriels!=null && this.telephones.length!=0){
            for (String s : this.courriels){
                sTrim+="\n"+s;
            }
        }
        return sTrim;
    }

    /** 
     * Permet d'ajouter un numéro de téléphone au contact. Si le téléphone entré est null, 
     * aucun changement n'est fait.
     * @param Telephone tel, le telephone à ajouter au contact.
     */
    public void ajouterTelephone(Telephone tel){
        if (tel !=null){
            Telephone[] tab=new Telephone[this.telephones.length+1];
            for (int i=0;i<this.telephones.length;i++){
                tab[i]=this.telephones[i];
            }
            tab[this.telephones.length]=tel;
            this.telephones=tab;
        }
    }

    /** 
     * Permet d'ajouter un courriel au contact. Si le courriel est null ou vide, aucun 
     * changement n'est fait.
     * @param String courriel, le courriel à ajouter au contact.
     */
    public void ajouterCourriel(String courriel){
        if (courriel != null && !courriel.isEmpty()){
            String[] tab= new String[this.courriels.length+1];
            for (int i=0;i<this.courriels.length;i++){
                tab[i]=this.courriels[i];
            }
            tab[this.courriels.length]=courriel;
            this.courriels=tab;
        }
    }

    /** 
     * Permet d'obtenir le telephone d'un contact à la position i souhaitée. Si la position
     * n'est pas valide, la méthode retourne null.
     * @param int position, la position dans le tableau du téléphone qu'on souhaite obtenir.
     * @return le telephone dont on souhaite connaitre la position, en type Telephone.
     */
    public Telephone obtenirTelephone(int position){
        Telephone tel=null;
        if (position<telephones.length){
            tel=telephones[position];
        }
        return tel;
    }

    /** 
     * Permet d'obtenir le courriel d'un contact à la position i souhaitée. Si la position
     * n'est pas valide, la méthode retourne null.
     * @param int position, la position dans le tableau du courriel qu'on souhaite obtenir.
     * @return le courriel dont on souhaite connaitre la position, en type String.
     */
    public String obtenirCourriel(int position){
        String courriel=null;
        if (position<courriels.length){
            courriel=courriels[position];
        }
        return courriel;
    }

    /** 
     * Permet de supprimer le telephone d'un contact à la position i souhaitée. Si la position
     * n'est pas valide, rien n'est supprimé et la méthode retourne null.
     * @param int position, la position dans le tableau du téléphone qu'on souhaite supprimer.
     * @return le téléphone que l'on vient de supprimer, en type Telephone.
     */
    public Telephone supprimerTelephone(int position){
        Telephone tel=null;
        if (position<telephones.length){
            Telephone [] tab=new Telephone[telephones.length-1];
            tel=this.telephones[position];
            for (int i=0;i<position;i++){
                tab[i]=this.telephones[i];
            }
            for (int i=position+1;i<this.telephones.length;i++){
                tab[i-1]=this.telephones[i];
            }
            this.telephones=tab;
        }
        return tel;
    }

    /** 
     * Permet de supprimer le courriel d'un contact à la position i souhaitée. Si la position
     * n'est pas valide, rien n'est supprimé et la méthode retourne null.
     * @param int position, la position dans le tableau du courriel qu'on souhaite supprimer.
     * @return le courriel que l'on vient de supprimer, en type String.
     */
    public String supprimerCourriel(int position){
        String courriel=null;
        if (position<courriels.length){
            String [] tab=new String[courriels.length-1];
            courriel=this.courriels[position];
            for (int i=0;i<position;i++){
                tab[i]=this.courriels[i];
            }
            for (int i=position+1;i<this.telephones.length;i++){
                tab[i-1]=this.courriels[i];
            }
            this.courriels=tab;
        }
        return courriel;
    }

    /** 
     * Permet de modifier le téléphone d'un contact à la position i souhaitée. Si la position
     * n'est pas valide, rien n'est modifié, et si un des champs n'est pas valide, alors il 
     * n'est pas modifié.
     * @param int position, la position dans le tableau du téléphone qu'on souhaite modifier.
     * @param String indReg, l'indice régional du téléphone
     * @param String numero, le numero du téléphone.
     * @param String poste, le poste du téléphone.
     * @param String type, le type du téléphone.
     */
    public void modifierTelephone(int position, String indReg, String numero, String poste, String type){
        if (position<telephones.length){
            telephones[position].setIndReg(indReg);
            telephones[position].setNumero(numero);
            telephones[position].setPoste(poste);
            telephones[position].setType(type);
        }
    }

    /** 
     * Permet de modifier le courriel d'un contact à la position i souhaitée. Si la position
     * n'est pas valide ou si le courriel est null ou vide, rien n'est modifié.
     * @param int position, la position dans le tableau du courriel qu'on souhaite supprimer.
     * @param String position, la nouvelle valeur de courriel que l'on souhaite modifier.
     */
    public void modifierCourriel(int position, String courriel){
        if (position<telephones.length && !(courriel==null) && courriel.length()!=0){
            this.courriels[position]=courriel;
        }
    }

    /** 
     * Permet d'obtenir le nombre de télephones enregistrés dans un contact.
     * @return un entier qui indique le nombre de téléphones enregistrés dans un contact.
     */
    public int nbrTelephones(){
        return this.telephones.length;
    }

    /** 
     * Permet d'obtenir le nombre de courriels enregistrés dans un contact.
     * @return un entier qui indique le nombre de courriels enregistrés dans un contact.
     */
    public int nbrCourriels(){
        return this.courriels.length;
    }

//-------------- Méthode de classe ------------------

    /** 
     * Permet d'obtenir le nombre de contacts favoris enregistrés.
     * @return un entier qui indique le nombre de contacts définis comme favori.
     */
    public static int obtenirNbrContactsFavoris(){
        return nbrContactsFavoris;
    }

}