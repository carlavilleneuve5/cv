# Projets Paris Hippiques

## Description

Ce projet a été réalisée dans le cadre du premier TP du cours de programmation Java à l'UQAM. 
Il a pour but de réaliser un mini-jeu de Paris Hippiques.

## Fonctionnement

Il suffit de se placer dans le dossier du projet puis de compiler et d'éxécuter
le programme de la facon suivante:

```sh
$ javac ParisHippiques.java
$ java ParisHippiques
```

## Dépendances

* Compilateur javac
* Classe Clavier.java qui gère les entrées claviers
* Classe TP1Utils.java qui contient des méthodes prédéfinies


