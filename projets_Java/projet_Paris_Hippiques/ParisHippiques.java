/**
 * ----------------------------------------------
 * ----------- Travail Pratique #1 --------------
 * ----------------------------------------------
 * 
 * @author Carla VILLENEUVE
 * Code permanent : VILC67520109
 * Courriel : cc191969@ens.uqam.ca
 * 
 * Cours : INF1120-30 - Programmation I
 * 
 * @version 2022-10-20
*/

import java.util.*;


public class ParisHippiques{

    public static final String MSG_INITIALISATION = "\nCe programme permet de placer des paris sur des courses hippiques virtuelles.\n";
    public static final String MSG_BANQUE_VIDE = "\nVotre banque est vide.";
    public static final String MSG_SAISIR_MONTANT_BANQUE = "Pour continuer, entrez un montant a mettre en banque (0 pour quitter) : ";
    public static final String MSG_ERREUR_SAISIE_MONTANT = "\nErreur, le montant doit etre plus grand ou egal a 0! Recommencez...";
    public static final String MSG_FIN_PROGRAMME = "\nAUREVOIR!\n";
    public static final String MSG_AFFICHAGE_MENU="\n----\nMENU\n----\n\n1. Placer un pari\n2. Gerer la banque\n3. Quitter\n\nEntrez un choix : ";
    public static final String MSG_ERREUR_SAISIE_MENU="\nErreur, entrez un choix entre 1 et 3! Recommencez...";
    public static final String MSG_GESTION_BANQUE="\n---------------\nGERER LA BANQUE\n---------------";
    public static final String MSG_AFFICHAGE_BANQUE="\n** Montant en banque : ";
    public static final String MSG_CHOIX_BANQUE="\n(A)jouter, (V)ider, ou (R)evenir au menu principal :";
    public static final String MSG_ERREUR_CHOIX_BANQUE="\nErreur, entrez A, V ou R! Recommencez...";
    public static final String MSG_AJOUTER_MONTANT="Entrez le montant a ajouter (0 pour annuler) :";
    public static final String MSG_AFFICHAGE_PARIS="\n--------------\nPLACER UN PARI\n--------------\n";
    public static final String MSG_AFFICHAGE_MENU_PARIS="Type de pari\n 1. Pari simple gagnant\n 2. Pari simple place\n 3. Pari couple gagnant ordonne\n 4. Pari couple gagnant non ordonne\n 5. Revenir au menu principal\n\nEntrez le type de pari : ";
    public static final String MSG_ERREUR_SAISIE_PARIS="\nErreur, entrez un choix entre 1 et 5! Recommencez...";
    public static final String MSG_MENU_CHEVAUX="\nChevaux\n 1. Gaspard\n 2. Bubulle\n 3. Babette\n 4. Socrate\n 5. Romarin\n 6. Canelle\n";
    public static final String MSG_SAISIE_CHEVAUX_CLASSIQUE="Entrez le numero du cheval : ";
    public static final String MSG_SAISIE_CHEVAUX_PREMIER="Entrez le numero du premier cheval : ";
    public static final String MSG_SAISIE_CHEVAUX_SECOND="Entrez le numero du deuxieme cheval : ";
    public static final String MSG_ERREUR_SAISIE_CHEVAUX="\nErreur, le numero du cheval doit etre entre 1 et 6! Recommencez...";
    public static final String MSG_MONTANT_MISE="Entrez le montant de la mise (0 pour annuler) : ";
    public static final String MSG_ERREUR_MONTANT_MISE="\nErreur, la mise doit etre entre 0.00$ et ";
    public static final String MSG_ANNULATION_OPERATION="\n\n----> OPERATION ANNULEE <----\n";
    public static final String MSG_ATTENTE_ENTREE="Appuyez sur <ENTREE> pour revenir au menu principal...";
    public static final String MSG_BRAVO="\nBRAVO ! Vous avez gagné";
    public static final String MSG_PERDU="\nDESOLE ! Vous avez perdu votre pari.\n";
    public static final String MSG_GAIN_CUMULE="GAIN CUMULE     : ";
    public static final String MSG_PERTE_CUMULE="PERTE CUMULE    : ";
    public static final String MSG_BANQUE="BANQUE          : ";

    public static void main (String [ ] args){

        // Déclarations

        double montantBanque;
        double montantAjoutee;
        double montantMise;
        double gainCumulee=0;
        char choixMenuPrincipal;
        char choixMenuBanque;
        char choixMenuParis;
        int choixMenuChevaux1;
        int choixMenuChevaux2; //Servira pour les paris sur 2 chevaux
        int classement;
        boolean finProgramme=false;
        boolean sortieGestionBanque;
        boolean sortieParis;

        // Initialisation
        System.out.println(MSG_INITIALISATION);
        System.out.println(MSG_BANQUE_VIDE);
        System.out.print(MSG_SAISIR_MONTANT_BANQUE);
        montantBanque = Clavier.lireDouble();

        // Gestion d'erreur si on saisit un montant négatif
        while(montantBanque<0){
            System.out.println(MSG_ERREUR_SAISIE_MONTANT);
            System.out.println(MSG_BANQUE_VIDE);
            System.out.print(MSG_SAISIR_MONTANT_BANQUE);
            montantBanque = Clavier.lireDouble();
        }
        // Si on saisit 0, on quitte le programme
        if (montantBanque==0){
        //Sinon on affiche le menu principal
        } else {
            // Tant qu'on ne souhaite pas quitter le programme, on affiche le menu principal
            while(!finProgramme){
                choixMenuBanque='0'; //On remet le caractère du choix de menu banque à 0 dans le cas où on sort du cas 2 avec la valeur 'r'
                System.out.print(MSG_AFFICHAGE_MENU);
                choixMenuPrincipal=Clavier.lireCharLn();
                switch(choixMenuPrincipal){
                    //Si l'utilisateur appuie sur 1 on rentre dans l'option Placer un pari
                    case '1':
                        sortieParis=false;
                        // Tanr qu'on ne veut pas sortir de l'option paris
                        while(!sortieParis){
                            System.out.println(MSG_AFFICHAGE_PARIS);
                            System.out.print(MSG_AFFICHAGE_MENU_PARIS);
                            choixMenuParis=Clavier.lireCharLn();
                            //On choisit le type de paris
                            switch(choixMenuParis){
                                //On regroupe les cas 1 et 2 afin d'optimiser le nombre de lignes de code
                                case '1','2':
                                    //On choisit le numéro du cheval
                                    System.out.println(MSG_MENU_CHEVAUX);
                                    System.out.print(MSG_SAISIE_CHEVAUX_CLASSIQUE);
                                    choixMenuChevaux1=Clavier.lireInt();
                                    //Gestion d'erreur si le cheval choisie n'est pas dans la liste
                                    while((choixMenuChevaux1<1) || (choixMenuChevaux1>6)){
                                        System.out.println(MSG_ERREUR_SAISIE_CHEVAUX);
                                        System.out.println(MSG_MENU_CHEVAUX);
                                        System.out.print(MSG_SAISIE_CHEVAUX_CLASSIQUE);
                                        choixMenuChevaux1=Clavier.lireInt();
                                    }
                                    //On choisit la mise 
                                    System.out.print(MSG_MONTANT_MISE);
                                    montantMise=Clavier.lireDouble();
                                    // Gestion d'erreur du montant de la mise
                                    while(montantMise<0 || montantMise>montantBanque){
                                        System.out.printf(Locale.CANADA,"%s %.2f $. Recommencez...%n",MSG_ERREUR_MONTANT_MISE,montantBanque);
                                        System.out.print(MSG_MONTANT_MISE);
                                        montantMise=Clavier.lireDouble();
                                    }
                                    //On deduit la mise du montant cumulee et du montant de la banque
                                    gainCumulee-=montantMise;
                                    montantBanque-=montantMise;
                                    //Si on appuie sur 0, on quitte le paris
                                    if (montantMise==0){
                                        System.out.println(MSG_ANNULATION_OPERATION);
                                        System.out.print(MSG_ATTENTE_ENTREE);
                                        Clavier.lireFinLigne();
                                        sortieParis=true;
                                    } else{
                                        //Sinon on lance la course
                                        System.out.println();
                                        classement=TP1Utils.executerCourse();
                                        //Si on a choisit le type de paris 1
                                        if (choixMenuParis=='1'){
                                            //Si le cheval est arrivé en 1ere position, on gagne le paris et on calcule les gains
                                            if ((classement / 100000) ==choixMenuChevaux1){
                                                System.out.printf(Locale.CANADA,"%s %.2f $ %n%n",MSG_BRAVO,montantMise*3);
                                                gainCumulee=gainCumulee+(montantMise*3);
                                                montantBanque=montantBanque+(montantMise*3);
                                            } else {
                                                System.out.println(MSG_PERDU);
                                            }
                                        //Si on a choisit le type de paris 2
                                        } else{
                                            //Si le cheval est arrivé en 1ere ou 2ème position, on gagne le paris
                                            if (((classement / 100000) ==choixMenuChevaux1) || ((classement / 10000 % 10) == choixMenuChevaux1)){
                                                System.out.printf(Locale.CANADA,"%s %.2f $%n%n",MSG_BRAVO,montantMise*2);
                                                gainCumulee=gainCumulee+(montantMise*2);
                                                montantBanque=montantBanque+(montantMise*2);
                                            }
                                            else{
                                                System.out.println(MSG_PERDU);
                                            }
                                        }
                                        //On affiche le gain ou la perte cumulee
                                        if (gainCumulee<0){
                                            System.out.printf(Locale.CANADA,"%s %.2f $ %n",MSG_PERTE_CUMULE,(-gainCumulee));
                                        } else{
                                            System.out.printf(Locale.CANADA,"%s %.2f $ %n",MSG_GAIN_CUMULE,gainCumulee);
                                        }
                                        //On affiche le montant en banque
                                        System.out.printf(Locale.CANADA,"%s %.2f $%n%n",MSG_BANQUE,montantBanque);
                                        System.out.print(MSG_ATTENTE_ENTREE);
                                        Clavier.lireFinLigne();
                                        //Si il reste de l'argent en banque, on retourne au menu principal
                                        if (montantBanque > 0){
                                            sortieParis=true;
                                        }
                                        //Sinon, on demande a l'utilisateur de rajouter de l'argent en banque
                                        else{
                                            System.out.println(MSG_BANQUE_VIDE);
                                            System.out.print(MSG_SAISIR_MONTANT_BANQUE);
                                            montantBanque = Clavier.lireDouble();
                                            // Gestion d'erreur si on saisit un montant négatif
                                            while(montantBanque<0){
                                                System.out.println(MSG_ERREUR_SAISIE_MONTANT);
                                                System.out.println(MSG_BANQUE_VIDE);
                                                System.out.print(MSG_SAISIR_MONTANT_BANQUE);
                                                montantBanque = Clavier.lireDouble();
                                            }
                                            //Si il tape 0, on termine le programme
                                            if (montantBanque==0){
                                                sortieParis=true;
                                                finProgramme=true;
                                            }
                                            //Sinon on retourne au menu principal avec le nouveau montant en banque
                                            else{
                                                sortieParis=true;
                                            }
                                        }
                                    }
                                break;
                                // On regroupe les cas 3 et 4 pour optimiser le nombre de lignes de code
                                case '3','4':
                                    //On choisit le premier cheval
                                    System.out.println(MSG_MENU_CHEVAUX);
                                    System.out.print(MSG_SAISIE_CHEVAUX_PREMIER);
                                    choixMenuChevaux1=Clavier.lireInt();
                                    //Gestion d'erreur du si le numero du cheval n'est pas valide
                                    while((choixMenuChevaux1<1) || (choixMenuChevaux1>6)){
                                        System.out.println(MSG_ERREUR_SAISIE_CHEVAUX);
                                        System.out.println(MSG_MENU_CHEVAUX);
                                        System.out.print(MSG_SAISIE_CHEVAUX_PREMIER);
                                        choixMenuChevaux1=Clavier.lireInt();
                                    }
                                    //On choisit le second cheval
                                    System.out.println(MSG_MENU_CHEVAUX);
                                    System.out.print(MSG_SAISIE_CHEVAUX_SECOND);
                                    choixMenuChevaux2=Clavier.lireInt();
                                    //Gestion d'erreur du si le numero du cheval n'est pas valide
                                    while((choixMenuChevaux2<1) || (choixMenuChevaux2>6)){
                                        System.out.println(MSG_ERREUR_SAISIE_CHEVAUX);
                                        System.out.println(MSG_MENU_CHEVAUX);
                                        System.out.print(MSG_SAISIE_CHEVAUX_SECOND);
                                        choixMenuChevaux2=Clavier.lireInt();
                                    }
                                    //On choisit la mise
                                    System.out.print(MSG_MONTANT_MISE);
                                    montantMise=Clavier.lireDouble();
                                    // Gestion d'erreur si la mise n'est pas valide
                                    while(montantMise<0 || montantMise>montantBanque){
                                        System.out.printf(Locale.CANADA,"%s %.2f $. Recommencez...%n",MSG_ERREUR_MONTANT_MISE,montantBanque);
                                        System.out.print(MSG_MONTANT_MISE);
                                        montantMise=Clavier.lireDouble();
                                    }
                                    //On deduit la mise du montant cumulee et du montant en banque
                                    gainCumulee-=montantMise;
                                    montantBanque-=montantMise;
                                    //Si l'utlisateur tape 0, on annule le paris
                                    if (montantMise==0){
                                        System.out.println(MSG_ANNULATION_OPERATION);
                                        System.out.print(MSG_ATTENTE_ENTREE);
                                        Clavier.lireFinLigne();
                                        sortieParis=true;
                                    } else{
                                        //Sinon on lance la course
                                        System.out.println();
                                        classement=TP1Utils.executerCourse();
                                        //Si on a choisit le type de paris 3
                                        if (choixMenuParis=='3'){
                                            // Si le cheval 1 arrive premier et le cheval 2 arrive deuxieme alors on gagne le paris
                                            if (((classement / 100000) ==choixMenuChevaux1) && ((classement / 10000 % 10) == choixMenuChevaux2)){
                                                System.out.printf(Locale.CANADA,"%s %.2f $ %n%n",MSG_BRAVO,montantMise*3.5);
                                                gainCumulee=gainCumulee+(montantMise*3.5);
                                                montantBanque=montantBanque+(montantMise*3.5);
                                            }
                                            else{
                                                System.out.println(MSG_PERDU);
                                            }
                                        // Si on a choisit le type de paris 4
                                        } else {
                                            // Si les 2 chevaux saisis arrivent dans les deux premieres positions de la course on gagne le paris
                                            if ((((classement / 100000) ==choixMenuChevaux1) && ((classement / 10000 % 10) == choixMenuChevaux2)) || 
                                            (((classement / 100000) ==choixMenuChevaux2) && ((classement / 10000 % 10) == choixMenuChevaux1))){
                                                System.out.printf(Locale.CANADA,"%s %.2f $ %n%n",MSG_BRAVO,montantMise*2.5);
                                                gainCumulee=gainCumulee+(montantMise*2.5);
                                                montantBanque=montantBanque+(montantMise*2.5);
                                            }
                                            else{
                                                System.out.println(MSG_PERDU);
                                            }
                                        }
                                        //On affiche le gain ou la perte cumulee
                                        if (gainCumulee<0){
                                            System.out.printf(Locale.CANADA,"%s %.2f $ %n",MSG_PERTE_CUMULE,(-gainCumulee));
                                        } else{
                                            System.out.printf(Locale.CANADA,"%s %.2f $ %n",MSG_GAIN_CUMULE,gainCumulee);
                                        }
                                        //On affiche le montant en banque
                                        System.out.printf(Locale.CANADA,"%s %.2f $ %n%n",MSG_BANQUE,montantBanque);
                                        System.out.print(MSG_ATTENTE_ENTREE);
                                        Clavier.lireFinLigne();
                                        if (montantBanque > 0){
                                            sortieParis=true;
                                        }
                                        //Si le montant en banque est vide, on demande un ajout d'argent
                                        else{
                                            System.out.println(MSG_BANQUE_VIDE);
                                            System.out.print(MSG_SAISIR_MONTANT_BANQUE);
                                            montantBanque = Clavier.lireDouble();
                                            // Gestion d'erreur si on saisit un montant négatif
                                            while(montantBanque<0){
                                                System.out.println(MSG_ERREUR_SAISIE_MONTANT);
                                                System.out.println(MSG_BANQUE_VIDE);
                                                System.out.print(MSG_SAISIR_MONTANT_BANQUE);
                                                montantBanque = Clavier.lireDouble();
                                            }
                                            // Si l'utilisateur choisit 0, on quitte le programme
                                            if (montantBanque==0){
                                                sortieParis=true;
                                                finProgramme=true;
                                            }
                                            // Sinon on retourne au menu principal
                                            else{
                                                sortieParis=true;
                                            }
                                        }                                       
                                    }
                                break;
                                case '5':
                                    // On retourne au menu principal
                                    sortieParis=true;
                                break;
                                // Pour tout autre cas, on affiche un message d'erreur et on redemande une saisie a l'utilisateur (en rebouclant le while)
                                default :
                                    System.out.println(MSG_ERREUR_SAISIE_PARIS);
                                break;
                            }
                        }
                    break;
                    //Si l'utilisateur appuie sur 2 on rentre dans l'option Gérer la banque                   
                    case '2':
                        sortieGestionBanque=false; // On remet la valeur à false dans le cas ou on est deja passé dans ce menu
                        System.out.println(MSG_GESTION_BANQUE);
                        while(!sortieGestionBanque){
                            // On affiche le montant en banque et on demande a l'utilisateur ce qu'il souhaite faire
                            System.out.printf(Locale.US,"%s %.2f $ ** %n",MSG_AFFICHAGE_BANQUE,montantBanque);
                            System.out.print(MSG_CHOIX_BANQUE);
                            choixMenuBanque=Clavier.lireCharLn();
                            switch(choixMenuBanque){
                                case 'a','A':
                                // On ajoute le montant saisie a la banque
                                    System.out.print(MSG_AJOUTER_MONTANT);
                                    montantAjoutee=Clavier.lireDouble();
                                    // Gestion d'erreur si le montant est invalide
                                    while(montantAjoutee<0){
                                        System.out.println(MSG_ERREUR_SAISIE_MONTANT);
                                        System.out.print(MSG_AJOUTER_MONTANT);
                                        montantAjoutee=Clavier.lireDouble();
                                    }
                                    montantBanque+=montantAjoutee;
                                break;
                                case 'r','R':
                                    //On revient au menu principal
                                    sortieGestionBanque=true;
                                break;
                                case 'v','V':
                                    // On quitte le programme
                                    sortieGestionBanque=true;
                                    finProgramme=true;
                                break;
                                //Pour tout autre cas, on affiche un message d'erreur et on demande une nouvelle saisie (en rebouclant sur le while)
                                default :
                                    System.out.println(MSG_ERREUR_CHOIX_BANQUE);
                                break;
                            }
                        }
                    break;
                    //Si l'utilisateur appuie sur 3 on quitte le programme
                    case '3':
                        finProgramme=true;
                    break;
                    //Pour tout autre cas, on affiche un message d'erreur
                    default:
                        System.out.println(MSG_ERREUR_SAISIE_MENU);
                    break;
                }
            }
        }
        System.out.println(MSG_FIN_PROGRAMME);
    }
}
