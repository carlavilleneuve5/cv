# Projet Indenteur pseudocode

## Description

Ce projet a été réalisé dans le cadre du second TP du cours de Programmation Java à l'UQAM.
Il a pour but de réaliser un Indenteur de pseudo-code. 
Un fichier de pseudo-code est entré à la console et le programme gère l'indentation
et la casse du fichier. 

## Fonctionnement

Il suffit de se placer dans le dossier du projet puis de compiler et d'éxécuter le 
programme IndenteurPseudocode.java comme ceci:

```sh 
$ javac IndenteurPseudocode.java
$ java IndenteurPseudocode
```

## Dépendances

* Compilateur javac 
* Classe Clavier.java qui gère les entrées claviers
* Classe TP2Utils.java qui contient des méthodes prédéfinies


