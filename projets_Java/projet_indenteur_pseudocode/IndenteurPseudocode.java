/**  
 * Cette classe represente un Indenteur de pseudo-code.
 * 
 * @author Carla Villeneuve 
 * Code permanent : VILC67520109 
 * Courriel : cc191969@ens.uqam.ca  
 * Cours : INF1120-30  
 * @version 2022-11-19 */

 public class IndenteurPseudocode{
    
    public static final String INITIALISATION=
       "\nCe programme permet de corriger l'indentation d'un algorithme ecrit en pseudocode.\n";
    public static final String MENU_PRINCIPAL="\n----\nMENU\n----\n1. Indenter pseudocode\n2. Quitter\n";
    public static final String SAISIE_CHOIX_MENU="Entrez votre choix :";
    public static final String ERR_SAISIE_MENU="\nERREUR ! Choix invalide. Recommencez...\n";
    public static final String SAISIE_CHEMIN="Entrez le chemin du fichier de pseudocode : ";
    public static final String ERR_CHEMIN="\nERREUR ! Chemin de fichier invalide. Recommencez...";
    public static final String SAISIE_CHOIX_FORMAT="Mots reserves : en (m)inuscules ou en (M)ajuscules :";
    public static final String ERR_SAISIE_FORMAT="\nERREUR ! Entrez m ou M. Recommencez...";
    public static final String TIRETS="--------------------";
    public static final String SAISIE_ENTREE="\nTapez <ENTREE> pour continuer...";
    public static final String FIN_PROGRAMME="\n\nF I N   N O R M A L E   D U   P R O G R A M M E\n";

        /**
    * Affiche le message d'initialisation du programme
    *
    * @param void
    * @return void
    */
    public static void afficherInitialisation(){
        System.out.println(INITIALISATION);
    }
        /**
    * Affiche le menu principal du programme
    *
    * @param void
    * @return void
    */
    public static void afficherMenu(){
        System.out.println(MENU_PRINCIPAL);
    }

        /**
    * Affiche le message de fin de programme
    *
    * @param void
    * @return void
    */
    public static void afficherFinProg(){
        System.out.println(FIN_PROGRAMME);
    }
        /**
    * Permet de valider le choix du menu principal. L'utilisateur saisit une valeur, est si elle 
    * n'est pas compris entre les bornes inférieur et supérieur inscrites en paramètre, affiche un
    * message d'erreur et demande la saisie d'une nouvelle valeur.
    *
    * @param String msg Soll est le message de solicitation choisie,
             String msg Err est le message d'erreur choisi pour indiquer l'utilisateur une mauvaise saisie
             char choixMenu1 est la borne inférieur de choix de menu
             char choixMenu2 est la borne supérieur de choix de menu
    * @return retourne le caractère saisie une fois validé.
    */
    public static char validerChoix(String msgSoll, String msgErr, char choix1, char choix2){
        String chaineSaisi;
        System.out.print(msgSoll);
        chaineSaisi=Clavier.lireString();
        while (chaineSaisi.length()!=1 || ((chaineSaisi.charAt(0)!=choix1) && (chaineSaisi.charAt(0)!=choix2))){
           System.out.println(msgErr);
           System.out.print(msgSoll);
           chaineSaisi=Clavier.lireString();
        }
        return chaineSaisi.charAt(0);
     }
        /**
    * Permet de valider le chemin du fichier. L'utilisateur saisit un chemin de fichier, et si la valeur n'est
    * pas validée par la fonction lireFichierTexte de TP2Utils, la méthode affiche un
    * message d'erreur et demande la saisie d'une nouvelle valeur. Une fois validé, la méthode retourne 
    * le contenu du fichier texte en chaine de caractère.
    *
    * @param String msg Soll est le message de solicitation choisie,
             String msg Err est le message d'erreur choisi pour indiquer l'utilisateur une mauvaise saisie
    * @return retourne le contenu du fichier texte.
    */
     public static String validerCheminFichier(String msgSoll, String msgErr){
        String cheminFichier;
        System.out.print(msgSoll);
        cheminFichier=Clavier.lireString();
        while(TP2Utils.lireFichierTexte(cheminFichier)==null){
            System.out.println(msgErr);
            System.out.print(msgSoll);
            cheminFichier=Clavier.lireString();
        }
        return TP2Utils.lireFichierTexte(cheminFichier);
     }

        /**
    * Cette methode retourne une nouvelle chaine de caracteres qui represente
    * le pseudocode donne en parametre, auquel on a enleve tous les caracteres
    * blancs au debut de chaque ligne, et tous les caracteres blancs superflus
    * a la fin de chaque ligne (en conservant cependant le caractere '\n' qui
    * marque la fin d'une ligne).
    *
    * @param pseudocode le pseudocode a traiter.
    * @return une nouvelle chaine representant le pseudocode donne en parametre,
    * auquel on a enleve tous les caracteres blancs au debut et a la fin
    * de chaque ligne (en conservant le caractere '\n' a la fin de chaque
    * ligne).
    */
    public static String trimer (String pseudocode) {
        String sTrim = ""; //pseudocode a retourner
        String sTmp;
        int debut = 0; //indice du debut de la ligne courante
        int fin; //indice de la fin de la ligne courante
        //parcourir les lignes, et enlever les caracteres blancs avant et après
        //chaque ligne sauf le \n a la fin de la ligne.
        pseudocode = pseudocode.trim() + "\n";
        fin = pseudocode.indexOf('\n',debut);
        while (fin != -1) {
            //extraire la ligne courante et enlever tous les caracteres blancs
            //en debut et fin de ligne
            sTmp = pseudocode.substring(debut, fin);
            if (sTmp.length()!=0){
                // On ne trime pas la chaine s'il s'agit d'une ligne vide afin de garder les sauts de ligne
                if (sTmp.compareTo("\n")!=0){
                    sTmp=sTmp.trim();
                }
                //concatener la ligne courante a la chaine a retourner, en ajoutant
                //le saut de ligne a la fin.
                sTrim = sTrim + sTmp + "\n";
            }
            //ajuster le debut de la ligne courante suivante
            debut = fin + 1;
            //trouver la fin de la ligne courante suivante
            fin = pseudocode.indexOf('\n', debut);

        }
        return sTrim;
    }
        /**
    * Permet de changer la casse de la chaine rentrée en paramètre selon le choix de format passé 
    * en paramètre. Si le choix de format est 'm' alors on met la chaine en miniscule, si le choix
    * est 'M' alors on met la chaine en majuscule.
    *
    * @param String chaine est la chaine dont on veut changer la casse,
             char choixFormat est le format choisie par l'utilisateur: m ou M.
    * @return retourne la chaine de caractere dont la casse a été changé.
    */
    public static String changementCasse(String chaine, char choixFormat){
        String sTmp;
        if (choixFormat=='m'){
            sTmp=chaine.toLowerCase();
        } else{
            sTmp=chaine.toUpperCase();
        }
        return sTmp;    
    }

        /**
    * Permet de gérer la casse du pseudocode selon le choix de format désiré par l'utilisateur. On
    * passe en paramètre le pseudocode dont on veut changer la casse et le choix de format de l'utilisateur.
    * Si le choix de format est m, tous les mots réservés seront mis en miniscule et si le choix de format 
    * est M, tous les mots réservés seront mis en majuscule.
    *
    * @param String pseudocode est le pseudocode dont on veut changer la casse,
             char choixFormat est le format choisie par l'utilisateur: m ou M.
    * @return retourne le pseudocode dont la casse a été changé sous forme de String.
    */

    public static String gestionCasse(String pseudocode, char choixFormat){
        String sTrim="";
        String sTmp="";
        int finLigne;
        int i=0;
        // Tant qu'on a pas parcouru tout le pseudocode
        while(i<pseudocode.length()){
            // Si la ligne est un commentaire, on ne touche pas a la casse de la ligne
            if (pseudocode.charAt(i)=='/' && pseudocode.charAt(i+1)=='/'){
                finLigne=pseudocode.indexOf("\n",i);
                sTmp=pseudocode.substring(i,finLigne);
                i=finLigne;
                sTrim=sTrim+sTmp+"\n";
            // Si on est dans une citation, on ne touche pas a la casse
            } else if (pseudocode.charAt(i)=='\"'){
                sTrim=sTrim+sTmp+pseudocode.charAt(i);
                do {
                    i++;
                    sTrim=sTrim+sTmp+pseudocode.charAt(i);
                } while(pseudocode.charAt(i)!='\"');
            } else{
                // Sinon on extrait chaque mot dans sTmp
                while(!Character.isWhitespace(pseudocode.charAt(i))){
                    sTmp=sTmp+pseudocode.charAt(i);
                    i++;
                }
                // On regarde si le mot est un mot reservé et on change la casse si besoin
                if (testMotReserve(sTmp)){
                    sTmp=changementCasse(sTmp, choixFormat);
                }
                sTrim=sTrim+sTmp+pseudocode.charAt(i);
            }
            i++;
            sTmp="";
        }
        return sTrim;
    }

        /**
    * Permet de vérifier si la chaine de caractère entrée en paramètre correspond à un mot resérvé. 
    *
    * @param String sTmp est la chaine de caractère à tester,
    * @return retourne un booléen à vrai si la chaine est un mot reservé.
    */

    public static boolean testMotReserve(String sTmp){
        boolean testMot=false;
        if (sTmp.equalsIgnoreCase("debut") || sTmp.equalsIgnoreCase("fin") || sTmp.equalsIgnoreCase("si")
        || sTmp.equalsIgnoreCase("sinon") || sTmp.equalsIgnoreCase("tant") || sTmp.equalsIgnoreCase("que")
        || sTmp.equalsIgnoreCase("ecrire")|| sTmp.equalsIgnoreCase("lire") || sTmp.equalsIgnoreCase("afficher")
        || sTmp.equalsIgnoreCase("saisir")|| sTmp.equalsIgnoreCase("faire") || sTmp.equalsIgnoreCase("alors")){
            testMot=true;
        }
        return testMot;
    }

        /**
    * Permet de tester si la chaine de caractère entrée en paramètre correspond à un début de bloc. 
    *
    * @param String sTmp est la chaine de caractère à tester,
    * @return retourne un booléen à vrai si la chaine est un début de bloc.
    */

    public static boolean testDebutBloc(String sTmp){
        boolean testBloc=false;
        // On enleve les espaces blancs
        String temp=withoutWhiteSpace(sTmp);
        // On met le texte en miniscule
        temp=temp.toLowerCase();
        if ((temp.startsWith("tantque") && temp.endsWith("faire")) || (temp.compareToIgnoreCase("faire")==0) 
         || (temp.startsWith("si") && temp.endsWith("alors"))){
            testBloc=true;
        }
        return testBloc;
    }

        /**
    * Permet de tester si la chaine de caractère entrée en paramètre correspond à la fois 
    * a un debut et a une fin de bloc, soit les 2 possibilités: sinon, sinon si ... alors.
    *
    * @param String sTmp est la chaine de caractère à tester,
    * @return retourne un booléen à vrai si la chaine est une fin de bloc.
    */

    public static boolean testDebutFinBloc(String sTmp){
        boolean testBloc=false;
        // On enleve les espaces blancs
        String temp=withoutWhiteSpace(sTmp);
        // On met le texte en miniscule
        temp=temp.toLowerCase();
        if (temp.compareToIgnoreCase("sinon")==0 || (temp.startsWith("sinonsi") && temp.endsWith("alors"))){
            testBloc=true;
        }
        return testBloc;
    }

        /**
    * Permet de convertir une chaine de caractere en la même chaine de caractere sans espaces blancs.
    *
    * @param String sTmp est la chaine de caractère à tester,
    * @return retourne la chaine de caractere passée en paramètre sans les espaces blancs.
    */

    public static String withoutWhiteSpace(String sTmp){
        String temp="";
        for (int i=0;i<sTmp.length();i++){
            // Si le caractere n'est pas un espace blanc, on l'ajoute a la variable temporaire
            if (!Character.isWhitespace(sTmp.charAt(i))){
                temp=temp+sTmp.charAt(i);
            }
        }
        return temp;
    }

        /**
    * Permet de tester si la chaine de caractère entrée en paramètre correspond à une fin de bloc. 
    *
    * @param String sTmp est la chaine de caractère à tester,
    * @return retourne un booléen à vrai si la chaine est une fin de bloc.
    */

    public static boolean testFinBloc(String sTmp){
        boolean testFinBloc=false;
        // On enleve les espaces blancs
        String temp=withoutWhiteSpace(sTmp);
        // On met le texte en miniscule
        temp=temp.toLowerCase();
        // On crée une variable temp2 pour supprimer le cas ou une variable s'appelle tantque
        String temp2=sTmp.toLowerCase();
        if ((temp.compareToIgnoreCase("fintantque")==0) || (temp.compareToIgnoreCase("finsi")==0)
         || (temp.startsWith("tantque")&& !temp2.startsWith("tantque"))){
            testFinBloc=true;
        }
        return testFinBloc;
    }


        /**
    * Permet d'ajouter un ou plusieurs niveaux d'intentation a la chaine passée en paramètre.
    *
    * @param String sTmp est la chaine de caractère à tester,
             int niveau, correpond au niveau d'intentation souhaité
    * @return retourne la chaine de caractere indenté du niveau souhaité.
    */   

    public static String ajoutNiveauIndentation(String sTmp,int niveau){
        String indentation="";
        for (int i=0;i<niveau;i++){
            // On indente de 3 espaces par niveau d'indentation
            indentation=indentation+"   ";
        }
        // On concatène l'indentation avec la ligne passé en caractère
        return indentation+sTmp+'\n';
    }

        /**
    * Permet de calculer l'indice de fin d'une ligne, en fonction du début de ligne entré en paramètre.
    *
    * @param String pseudocode est le pseudocode a partir duquel on veut trouver les indices de ligne,
             int debut, correpond à l'indice de début de la ligne
    * @return retourne l'indice de fin de la ligne.
    */   

    public static int finNouvelleLigne(String pseudocode,int debut){
        int fin=pseudocode.indexOf("\n",debut);
        return fin;
    }
   
        /**
    * Permet d'extraire la ligne du pseudocode a partir de son indice de début et de fin.
    *
    * @param String pseudocode est le pseudocode a partir duquel on veut extraire la ligne,
             int debut, correpond à l'indice de début de la ligne
             int fin, correspond à l'indice de fin de ligne 
    * @return retourne la chaine de caractere extraite.
    */

    public static String nouvelleLigne(String pseudocode,int debut, int fin){
        return pseudocode.substring(debut,fin);
    }

       /**
    * Cette méthode parcourt le pseudocode passé en paramètre et gère l'indentation
    * en testant a chaque ligne les différents tests de début/fin de bloc.
    *
    * @param String pseudocode est le pseudocode a partir duquel on veut extraire la ligne,
    * @return retourne le pseudocode correctement indenté.
    */

    public static String gestionIndentation(String pseudocode){
        String sTrim="";
        String sChgt;
        String sTmp;
        String temp;
        int debut=0;
        int fin;
        int niveauIndentation=0;
        fin=pseudocode.indexOf("\n",debut);
        sTmp=pseudocode.substring(debut,fin);
        temp=sTmp.toLowerCase();
        // On vérifie que le code commence par debut
        if (temp.startsWith("debut")){
            sTrim=sTrim+ajoutNiveauIndentation(sTmp,niveauIndentation);
            // On incrémente le niveau d'intentation
            niveauIndentation++;
            debut=fin+1;
            // On regarde la ligne suivante
            fin=finNouvelleLigne(pseudocode, debut);
            sTmp=nouvelleLigne(pseudocode,debut,fin);
            // Tant qu'on a pas trouvé le mot fin 
            while(sTmp.compareToIgnoreCase("fin")!=0){
                // On regarde si on est a la fois sur une début/fin de bloc
                if (testDebutFinBloc(sTmp)){
                    niveauIndentation--;
                    sTrim=sTrim+ajoutNiveauIndentation(sTmp,niveauIndentation);
                    niveauIndentation++;
                // On regarde si on est sur un debut de bloc                    
                } else if (testDebutBloc(sTmp)){
                    sTrim=sTrim+ajoutNiveauIndentation(sTmp,niveauIndentation);
                    niveauIndentation++;
                // On regarde si on est sur une fin de bloc
                } else if (testFinBloc(sTmp)){
                    niveauIndentation--;
                    sTrim=sTrim+ajoutNiveauIndentation(sTmp,niveauIndentation);
                // Sinon on ajoute la nouvelle ligne avec le niveau d'indentation courant
                } else{
                    sTrim=sTrim+ajoutNiveauIndentation(sTmp,niveauIndentation);
                }
                // On regarde la ligne suivante
                debut=fin+1;
                fin=finNouvelleLigne(pseudocode, debut);
                sTmp=nouvelleLigne(pseudocode,debut, fin);
            }
            sTrim=sTrim+sTmp;
        }
        return sTrim;
    }

    public static void main (String [ ] args){

        char saisieChoixMenu;
        char saisieChoixFormat;
        boolean finProgramme=false;
        String contenuFichier;
        String contenuFichierTrim;
        String contenuFichierCasse;
        String contenuFichierIndente;
        int fromIndex=0;

        afficherInitialisation();
        while(!finProgramme){
            afficherMenu();
            saisieChoixMenu=validerChoix(SAISIE_CHOIX_MENU, ERR_SAISIE_MENU, '1', '2');
            if (saisieChoixMenu=='1'){
                contenuFichier=validerCheminFichier(SAISIE_CHEMIN, ERR_CHEMIN);
                saisieChoixFormat=validerChoix(SAISIE_CHOIX_FORMAT, ERR_SAISIE_FORMAT, 'm', 'M');
                contenuFichierTrim=trimer(contenuFichier);
                contenuFichierCasse=gestionCasse(contenuFichierTrim, saisieChoixFormat);
                contenuFichierIndente=gestionIndentation(contenuFichierCasse);
                System.out.println("\n" + TIRETS);
                System.out.println(contenuFichierIndente);
                System.out.println(TIRETS);
                System.out.print(SAISIE_ENTREE);
                Clavier.lireFinLigne();
            } else{
                finProgramme=true;
            }
        }
        afficherFinProg();
    }
 }