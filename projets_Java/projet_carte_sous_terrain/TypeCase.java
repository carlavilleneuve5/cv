/**
 * L'énumération TypeCase liste les 4 valeurs possibles d'une case: 
 * pleine, vide, porte verticale ou porte horizontale.
 * 
 * @author Carla Villeneuve (VILC67270107)
 * @version 14/02/2023
 */

public enum TypeCase{

    CASE_PLEINE('O'),
    CASE_VIDE('.'),
    PORTE_VERTICALE('|'),
    PORTE_HORIZONTALE('-'),
    ;

    private char valeurCase;

    // Constructeur

    private TypeCase (char valeurCase){
        this.valeurCase = valeurCase;
    }

    // Accesseur public

    public char getValeurCase(){
        return this.valeurCase;
    }

    //Méthode

    /**
     * Redéfinition de la méthode toString(), adapté à un TypeCase
     * 
     * @return la description d'un TypeCase sous forme d'une chaîne de caractère.
     */

    @Override
    public String toString(){
        return " type case = " + this.valeurCase;
    }
}