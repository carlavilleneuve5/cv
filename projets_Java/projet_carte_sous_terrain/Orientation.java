/**
 * L'énumération Orientation liste les 4 positions possibles d'un mur relatif à une pièce: 
 * haut, bas, droite ou gauche.
 * 
 * @author Carla Villeneuve (VILC67270107)
 * @version 15/02/2023
 */

public enum Orientation{

    HAUT,
    BAS,
    DROITE,
    GAUCHE,
    ;
}