/**
 * Cette classe représente une case sur la carte.
 * Elle possède une positionX, une positionY et une valeur.
 * 
 * @author Carla Villeneuve (VILC67270107)
 * @version 15/02/2023
 */

public class Case{

    private TypeCase valeur;
    private int positionX;
    private int positionY;

    //Constructeurs

    public Case(){
        this.positionX = 0;
        this.positionY = 0;
        this.valeur    = TypeCase.CASE_VIDE;
    }

    public Case(int positionX, int positionY, TypeCase valeur){
        this.positionX = positionX;
        this.positionY = positionY;
        this.valeur = valeur;
    }

    // Accesseurs publics

    public int getPositionX (){
        return this.positionX;
    }

    public int getPositionY(){
        return this.positionY;
    }

    public TypeCase getValeur(){
        return this.valeur;
    }

    // Modificateurs publics

    public void setPositionX (int positionX){
        this.positionX = positionX;
    }

    public void setPositionY (int positionY){
        this.positionY = positionY;
    }

    public void setValeur (TypeCase valeur){
        this.valeur = valeur;
    }

    // Méthodes

    /**
     * Redéfinition de la méthode toString(), adapté à une case
     * 
     * @return la description d'une pièce sous forme d'une chaîne de caractère.
     */
    @Override
    public String toString(){
        return "Case = [ position X = " + this.positionX + ", position Y = " + this.positionY + ", valeur = " + this.valeur.toString() + " ]";
    }
}