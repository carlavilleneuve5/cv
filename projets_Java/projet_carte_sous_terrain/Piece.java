/**
 * Cette classe représente une pièce.
 * Elle possède une taille en base, une taille en hauteur et un tableau
 * de cases de 2 dimensions.
 * 
 * @author Carla Villeneuve (VILC67270107)
 * @version 15/02/2023
 */

public class Piece{

    protected int base;
    protected int hauteur;
    protected Case[][] cases;

    // Constructeur

    public Piece(int base, int hauteur){
        this.base = base;
        this.hauteur = hauteur;
        this.cases = new Case[this.hauteur][this.base];
    }

    // Accesseurs publics

    public int getBase(){
        return this.base;
    }

    public int getHauteur(){
        return this.hauteur;
    }

    public Case[][] getCases(){
        return this.cases;
    }

    public Case getCase(int x, int y){
        return this.cases[x][y];
    }

    // Modificateurs publics

    public void setBase(int base){
        this.base = base;
    }

    public void setHauteur(int hauteur){
        this.hauteur = hauteur;
    }

    public void setOneCase(int x, int y, Case newCase){
        this.cases[x][y] = newCase;
    }

    // Méthode

    /**
     * Redéfinition de la méthode toString(), adapté à une pièce.
     * 
     * @return la description d'une pièce sous forme d'une chaîne de caractère.
     */

    @Override
    public String toString(){
        return "Piece = [ base = " + this.base + ", hauteur = " + this.hauteur + "]";
    }

}