# Projet Carte sous-terraine

## Description

Ce projet a été réaliser dans le cadre du TP1 du module de
Programmation Java II de l'UQAM. Il consiste à récupérer des
dimensions de pièce dans un fichier et à placer aléatoirement
sur une carte de dimension choisie. Une fois la pièce centrale 
placée, les autres pièces doivent automatiquement être construites
à l'abord de pièces déjà existantes en utilisant des portes.

## Fonctionnement

Il suffit de se placer dans le répertoire du projet et de lancer la commande make suivante:

```sh
$ make
$ javac.Principal
$ java Principal
$ Ce programme génère la carte d un antre sous-terrain.
$
$ Saisir le nom du fichier à lire et appuyer sur Entrée : test1.txt
```
Vous devez saisir manuellement le nom du fichier (par exemple test1.txt)
et le programme affichera la carte sur la sortie standard.

## Dépendances

* Compilateur javac
