/**
 * La classe Principal génère la carte d'un antre souterrain.
 * 
 * Il demande de saisir un nom de fichier valide qui contient les dimensions des pièces à placer.
 * Ensuite, il construit une carte de la taille des 2 premières valeurs du fichier. Il place
 * une première pièce au centre de la carte et place toutes les autres pièces aléatoirement.
 * Si la pièce ne recouvre pas une autre pièce, elle est placée, sinon on essaye de la déplacer.
 * Si même avec le déplacement, on ne peut pas placer la pièce, alors on a un échec.
 * Le programme se termine quand toutes les pièces ont été placées ou quand on a eu 100 échecs.
 * 
 * @author Carla Villeneuve (VILC67270107)
 * @version 15/02/2023
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Principal{

    public static final String INTRODUCTION = "Ce programme génère la carte d'un antre sous-terrain.\n";
    public static final String SAISIE_FICHIER = "Saisir le nom du fichier à lire et appuyer sur Entrée: ";

    public static ArrayList<Piece> piecesNonPlacées;
    public static ArrayList<Mur> murSansPortes;
    public static PiecePrincipal piecePrincipal;
    public static int offsetBase;
    public static int offsetHauteur;

    /**
     * Détermine si la pièce que l'on veut placer recouvre une piece déjà existante
     *
     * @param numPiece l'indice de la pièce qu'on essaye de placer
     * @return un booléen à true s'il y a déjà une pièce existante sur ces coordonnées
     *                      false s'il n'y a pas de pièce existante sur ces coordonnées
     */
    public static boolean pieceExistante(int numPiece){
        boolean deplacementPiece = false;
        for (int k = offsetHauteur - 1; k < offsetHauteur + piecesNonPlacées.get(numPiece).getHauteur() + 1; k++){
            for (int j = offsetBase - 1; j < offsetBase + piecesNonPlacées.get(numPiece).getBase() + 1; j++){
                if (!((k < piecePrincipal.getHauteur() && j < piecePrincipal.getBase() && k >= 0 && j >= 0 && 
                    piecePrincipal.getCase(k,j).getValeur() == TypeCase.CASE_PLEINE) ||
                    (k == piecePrincipal.getHauteur() || k == -1 || j ==piecePrincipal.getBase() || j == -1))){
                        deplacementPiece = true;
                }
            }
        }
        return deplacementPiece;
    }

    /**
     * Calcule la valeur en X à laquelle on doit placer une pièce.
     * @param numMur        l'indice du mur à côté duquel on va placer la pièce
     * @param numCaseMur    l'indice de la case du mur sur lequel on va placer la porte
     * @param numPiece      l'indice de la pièce que l'on souhaite placer.
     * @param numCasePiece  la case de la nouvelle pièce à côté de laquelle on va placer la porte
     * @return la valeur de l'offset en X
     */

    public static int calculOffsetBase(int numMur, int numCaseMur, int numPiece, int numCasePiece){
        int offsetBase = 0;
        switch (murSansPortes.get(numMur).getOrientation()){
            case DROITE:
                offsetBase = murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionX() + 1;
            break;
            case GAUCHE:
                offsetBase = murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionX() -
                            piecesNonPlacées.get(numPiece).getBase();
            break;
            case HAUT, BAS:
                offsetBase = murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionX() - numCasePiece;
            break;
        }
        return offsetBase;
    }

    /**
     * Calcule la valeur en Y à laquelle on doit placer une pièce.
     *
     * @param numMur        l'indice du mur à côté duquel on va placer la pièce
     * @param numCaseMur    l'indice de la case du mur sur lequel on va placer la porte
     * @param numPiece      l'indice de la pièce que l'on souhaite placer.
     * @param numCasePiece  la case de la nouvelle pièce à côté de laquelle on va placer la porte
     * @return la valeur de l'offset en Y
     */

    public static int calculOffsetHauteur(int numMur, int numCaseMur, int numPiece, int numCasePiece){
        int offsetHauteur = 0;
        switch (murSansPortes.get(numMur).getOrientation()){
            case DROITE,GAUCHE:
                offsetHauteur = murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionY() - numCasePiece;
            break;
            case HAUT:
                offsetHauteur = murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionY() -
                                piecesNonPlacées.get(numPiece).getHauteur();
            break;
            case BAS:
                offsetHauteur = murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionY() + 1;
            break;
        }
        return offsetHauteur;
    }

    /**
     * Ajoute la pièce centrale à la carte.
     *
     * Place la première pièce au centre de la carte. Elle ajoute
     * les 4 murs créés à la liste des murs sans porte puis supprime la pièce de
     * la liste des pièces non placées.
     *
     */

    public static void ajoutPieceCentrale(){

        int k;
        int j = 0;
        offsetHauteur = (piecePrincipal.getHauteur()/2) - (piecesNonPlacées.get(0).getHauteur()/2);
        offsetBase    = (piecePrincipal.getBase()/2) - (piecesNonPlacées.get(0).getBase()/2);
        if (offsetHauteur >= 0 && offsetBase >= 0) {
            for (k = offsetHauteur; k < offsetHauteur + piecesNonPlacées.get(0).getHauteur(); k++){
                for (j = offsetBase; j < offsetBase + piecesNonPlacées.get(0).getBase(); j++){
                    piecePrincipal.getCase(k, j).setValeur(TypeCase.CASE_VIDE);
                }
            }

            // Ajout des 4 murs à la liste des murs sans portes

            murSansPortes.add(new Mur(piecesNonPlacées.get(0).getBase(),offsetBase, offsetHauteur - 1, Orientation.HAUT));
            murSansPortes.add(new Mur(piecesNonPlacées.get(0).getBase(),offsetBase, k ,Orientation.BAS));
            murSansPortes.add(new Mur(piecesNonPlacées.get(0).getHauteur(), offsetBase - 1, offsetHauteur,Orientation.GAUCHE));
            murSansPortes.add(new Mur(piecesNonPlacées.get(0).getHauteur(), j , offsetHauteur,Orientation.DROITE));

            //Suppression de la piece placée de la liste des pièces non placées.
            piecesNonPlacées.remove(0);
        } else {
            piecePrincipal.afficherCarte();
            System.err.println("La piece centrale dépasse la taille de la carte.\n" +
                                " Aucune pièce n'a été placée.");
            System.exit(-1);
        }
    }

    /**
     * Déplace une pièce de différentes façons afin ne plus recouvrir une autre pièce.
     * Si la pièce est à gauche ou à droite d'une autre pièce, on déplace la pièce de haut
     * en bas, et si la pièce est en haut ou en bas d'une autre pièce, on la déplace de
     * droite à gauche. Si malgré le déplacement, on recouvre toujours une pièce, on renvoie
     * un booléen à faux.
     *
     * @param numCasePiece la case de la nouvelle pièce à côté de laquelle on va placer la porte
     * @param numMur       l'indice du mur à côté duquel on va placer la pièce
     * @param numCaseMur   l'indice de la case du mur sur lequel on va placer la porte
     * @param numPiece     l'indice de la pièce que l'on souhaite placer.
     * @param taillePiece  la taille de la pièce à placer (hauteur si le mur est verticale, largeur sinon)
     * @return booléen à faux si on a trouvé un emplacement pour la pièce,
     *         booléen à vrai si on n'a pas trouvé un emplacement pour la pièce
     */
    public static boolean déplacerPiece(int numCasePiece,
                                     int numMur,
                                     int numCaseMur,
                                     int numPiece,
                                     int taillePiece){

        boolean deplacementPiece = true;
        while (numCasePiece >= 0 && deplacementPiece){
            numCasePiece--;
            offsetBase = calculOffsetBase(numMur, numCaseMur, numPiece, numCasePiece);
            offsetHauteur = calculOffsetHauteur(numMur, numCaseMur, numPiece, numCasePiece);
            deplacementPiece = pieceExistante(numPiece);
        }
        while (numCasePiece < taillePiece && deplacementPiece) {
            numCasePiece++;
            offsetBase = calculOffsetBase(numMur, numCaseMur, numPiece, numCasePiece);
            offsetHauteur = calculOffsetHauteur(numMur, numCaseMur, numPiece, numCasePiece);
            deplacementPiece = pieceExistante(numPiece);
        }
        return deplacementPiece;
    }

    /**
     * Ajoute une pièce à la carte.
     *
     * @param numMur       l'indice du mur à côté duquel on va placer la pièce
     * @param numCaseMur   l'indice de la case du mur sur lequel on va placer la porte
     * @param numPiece     l'indice de la pièce que l'on souhaite placer.
     */

    public static void ajoutPiece(int numMur, int numCaseMur, int numPiece){

        int k = 0;
        int j = 0;
        // On ajoute la porte
        if (murSansPortes.get(numMur).isVertical()){
            piecePrincipal.getCase(murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionY(),
                    murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionX()).setValeur(TypeCase.PORTE_VERTICALE);
        } else {
            piecePrincipal.getCase(murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionY(),
                    murSansPortes.get(numMur).getCases().get(numCaseMur).getPositionX()).setValeur(TypeCase.PORTE_HORIZONTALE);
        }

        // On ajoute la piece
        for (k = offsetHauteur; k < offsetHauteur + piecesNonPlacées.get(numPiece).getHauteur(); k++){
            for (j = offsetBase; j < offsetBase + piecesNonPlacées.get(numPiece).getBase(); j++){
                piecePrincipal.getCase(k, j).setValeur(TypeCase.CASE_VIDE);
            }
        }
        // Ajout des nouveaux murs sans portes à la liste
        switch(murSansPortes.get(numMur).getOrientation()){
            case HAUT :
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getBase(),offsetBase, offsetHauteur - 1, Orientation.HAUT));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getHauteur(), offsetBase - 1, offsetHauteur,Orientation.GAUCHE));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getHauteur(), j , offsetHauteur,Orientation.DROITE));
                break;
            case BAS :
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getBase(),offsetBase, k ,Orientation.BAS));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getHauteur(), offsetBase - 1, offsetHauteur,Orientation.GAUCHE));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getHauteur(), j , offsetHauteur,Orientation.DROITE));
                break;
            case DROITE:
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getBase(),offsetBase, offsetHauteur - 1, Orientation.HAUT));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getBase(),offsetBase, k ,Orientation.BAS));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getHauteur(), j , offsetHauteur,Orientation.DROITE));
                break;
            case GAUCHE :
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getBase(),offsetBase, offsetHauteur - 1, Orientation.HAUT));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getBase(),offsetBase, k ,Orientation.BAS));
                murSansPortes.add(new Mur(piecesNonPlacées.get(numPiece).getHauteur(), offsetBase - 1, offsetHauteur,Orientation.GAUCHE));
                break;
        }
        // Suppression de la piece à placer et du mur sans porte de la liste
        piecesNonPlacées.remove(numPiece);
        murSansPortes.remove(numMur);
    }

    /**
     * Fonction main
     */

    public static void main (String [] args){

        String nomFichier;
        Scanner scIn;
        Scanner scFile = null;
        piecesNonPlacées = new ArrayList<Piece>();
        murSansPortes = new ArrayList<Mur>();
        piecePrincipal = new PiecePrincipal();
        int i = 0;
        int k = 0, j = 0;
        int echec = 0;
        Random r;
        int numMur, numPiece, numCaseMur, numCasePiece;
        boolean deplacementPiece = false;

        System.out.println(INTRODUCTION);
        scIn = new Scanner(System.in);
        System.out.print(SAISIE_FICHIER);
        nomFichier = scIn.next();
        System.out.println();

        try {
            scFile = new Scanner( new File(nomFichier));

            // Ajout des dimensions de la pièce principale

            piecePrincipal.setBase(scFile.nextInt());
            piecePrincipal.setHauteur(scFile.nextInt());

            // Ajout des pièces à la liste de Pieces Non placées

            while(scFile.hasNextInt()){
                piecesNonPlacées.add(new Piece(scFile.nextInt(),scFile.nextInt()));
                i++;
            }

            piecePrincipal.initializeCarte();
            ajoutPieceCentrale();

            r = new Random();

            while (piecesNonPlacées.size() != 0 && echec != 100){

                // Choix d'un mur au hasard
                numMur = r.nextInt(murSansPortes.size());

                // Choix d'une piece à placer au hasard
                numPiece = r.nextInt(piecesNonPlacées.size());

                // Choix d'une case du mur au hasard
                numCaseMur = r.nextInt(murSansPortes.get(numMur).getTaille());

                // Choix d'une case de la piece au hasard

                int taillePiece;
                if (murSansPortes.get(numMur).isVertical()){
                    taillePiece = piecesNonPlacées.get(numPiece).getHauteur();
                    numCasePiece = r.nextInt(taillePiece);

                } else {
                    taillePiece = piecesNonPlacées.get(numPiece).getBase();
                    numCasePiece = r.nextInt(taillePiece);
                }

                // Calcul des offsets
                offsetBase = calculOffsetBase(numMur, numCaseMur, numPiece, numCasePiece);
                offsetHauteur = calculOffsetHauteur(numMur, numCaseMur, numPiece, numCasePiece);

                // On regarde si on empiète sur une piece deja existante
                deplacementPiece = pieceExistante(numPiece);

                // On essaye de déplacer la piece si besoin
                if (deplacementPiece){
                    deplacementPiece = déplacerPiece(numCasePiece, numMur, numCaseMur, numPiece, taillePiece);
                }

                // La pièce ne peut pas être placée malgré les déplacements, c'est un échec
                if (deplacementPiece){
                    echec++;
                // Sinon, on ajoute la pièce
                } else {
                    ajoutPiece(numMur, numCaseMur, numPiece);
                }
            }
            piecePrincipal.afficherCarte();
        } catch (FileNotFoundException e) {
            System.err.println("Erreur: Fichier non trouvé.");
            System.exit(-1);
        } finally {
            scFile.close();
        }
    }
}