/**
 * Cette classe représente une pièce principale de la map.
 * Elle hérite de la classe Pièce et possède alors une taille en base,
 * une taille en hauteur et un tableau de Case de 2 dimensions.
 * 
 * @author Carla Villeneuve (VILC67270107)
 * @version 15/02/2023
 */

public class PiecePrincipal extends Piece{

    // Constructeurs

    public PiecePrincipal(int base, int hauteur){
        super(base,hauteur);
    }

    public PiecePrincipal(){
        super(1,1);
    }

    //Méthodes

    /**
     * Initialise une carte remplie de case pleine.
     */

    public void initializeCarte(){
        this.cases = new Case[this.hauteur][this.base];
        for (int i = 0; i < this.hauteur; i++){
            for(int j = 0; j < this.base; j++){
                this.cases[i][j] = new Case(i,j,TypeCase.CASE_PLEINE);
            }
        }
    }

    /**
     * Affiche la carte sur stdout.
     */

    public void afficherCarte(){
        for (int i = 0; i < this.hauteur; i++){
            for(int j= 0; j < this.base; j++){
                System.out.print(cases[i][j].getValeur().getValeurCase());
            }
            System.out.println();
        }
    }
}