/**
 * Cette classe représente un mur.
 * Le mur possède une taille, une orientation et un tableau de case.
 * 
 * @author Carla Villeneuve (VILC67270107)
 * @version 15/02/2023
 */

import java.util.*;

public class Mur {
    
    private int taille;
    private Orientation orientation;
    private ArrayList<Case> cases;

    // Constructeur

    public Mur(int taille, int positionX, int positionY, Orientation orientation){
        this.taille = taille;
        cases = new ArrayList<Case>(taille);
        this.orientation = orientation;
        if (orientation == Orientation.DROITE || orientation == Orientation.GAUCHE){
            for (int i = 0; i < taille; i++){
                cases.add(new Case(positionX,positionY + i, TypeCase.CASE_PLEINE));
            }
        } else {
            for (int i = 0; i < taille; i++){
                cases.add(new Case(positionX + i,positionY, TypeCase.CASE_PLEINE));
            }
        }
    }

    //Accesseur public

    public int getTaille(){
        return this.taille;
    }

    public ArrayList<Case> getCases(){
        return cases;
    }

    public Orientation getOrientation(){
        return this.orientation;
    }

    //Modificateur public

    public void setTaille(int taille){
        this.taille = taille;
    }

    public void setCase(int positionX, int positionY, int index){
        this.cases.get(index).setPositionX(positionX);
        this.cases.get(index).setPositionY(positionY);
    }

    // Méthodes

    /**
     * Indique si le mur est vertical ou non.
     * 
     * @return un booléen à vrai si le mur est vertical, et à faux si le mur est horizontal.
     */

    public boolean isVertical(){
        return (this.orientation == Orientation.DROITE) || (this.orientation == Orientation.GAUCHE);
    }

    /**
     * Redéfinition de la méthode toString(), adapté à un mur.
     * 
     * @return la description d'un mur sous forme d'une chaîne de caractère.
     */

    @Override
    public String toString(){
        return "MurSansPorte = [ taille = " + taille + ", orientation =  " + orientation + 
                ", case0: " + cases.get(0).toString() + " ]";
    }
    
}