%------------------------------------------------------------------------%
%-----------------------------Main PCG-----------------------------------%
%------------------------------------------------------------------------%

close all;
clear all

%First part--------------------------------------------------------------%
%Loading a phonocardiogram-----------------------------------------------%
fileName="phonocomplet_8k.wav";
[pcg,fs]=audioread(fileName);

%Affichage dans le domaine temporel--------------------------------------%
ts=1/fs;
t=[0:length(pcg)-1].*ts;
figure(1);
plot(t,pcg);
title("PCG temporel");
xlabel("Time (en s)");
hold on;

%% Retrouver le bpm-------------------------------------------------------%

%Plot dans le domaine temporel-------------------------------------------%
valeur_max=max(pcg);
j=0;
n=1;
p=1;
k=0;
liste(n)=0;
seuil=(2/3)*valeur_max;
min_s1 = 0.25*fs; %Temps min dun cycle systole + diastole

for j=1:length(pcg)
    if pcg(j) > seuil
        if j > k+min_s1
        liste(n)=(j);
        n=n+1;
        k=j;
        end
    end
end

total=mean(diff(liste));
bpm=1/(total*1/fs)*60;

disp('Le BPM est :');
disp(bpm);
 

%% Plot dans le domaine frequentiel---------------------------------------%

f_pcg=fft(pcg);
f=[-fs/2:fs/length(pcg):fs/2-(fs/(length(pcg)))];
figure(2);
plot(f,10*log10(fftshift(abs(f_pcg))));
xlim([0 4000]);
title("PCG in frequency domain");
xlabel("Frequency (in Hz)");
ylabel("Amplitude (en dB)");

%% Filtering with Butterworth---------------------------------------------%

Fp = 4000; 
Fa = 1000;
Wp = 2*Fp*pi;   % Band pass
Ws = 2*Fa*pi;   % Band stop
Rp=3;
Rs=20;
[n,Wn]=buttord(Wp,Ws,Rp,Rs,'s');
fprintf("%d",n);
[z,p] = butter(n,Wn,'s');
[bb ab] = bilinear(z, p, fs);
L = 32;
[hb,wb] = freqz(bb,ab,L);
% figure(1);
% plot(wb,20*log10(abs(hb)),'g'); 
title('PCG avant/après filtre de Butterworth');
ylabel('y (en dB)');
xlabel('Fréquence f (en Hz)');
outputdata=filter(bb,ab,pcg);
%figure();
hold on;
plot(f,10*log10(fftshift(abs(fft(outputdata)))));
legend('Signal non filtré','Signal filtré');
xlim([0 4000]);

%% Filtering with FIR ----------------------------------------------------%

yf=filter(bb,1,pcg);
y = yf ./ sqrt(sum(abs(bb).^2));

%% Downsampling ----------------------------------------------------------%
ovS=4;
fd = fs  / ovS;
y_down=downsample(y,ovS);
f_down=[-fd/2:fd/length(y_down):fd/2-(fd/(length(y_down)))];

figure(3);
plot(f,10*log10(fftshift(abs(f_pcg)/sqrt(ovS))));
title('PCG avant/après filtre FIR');
ylabel('y (en dB)');
xlabel('Fréquence f (en Hz)');
hold on;
plot(f_down,10*log10(fftshift(abs(fft(y_down)))));
legend('Signal non filtré','Signal filtré et sous échantillonné');
xlim([0 4000]);

%% Denoising - Sahnnon energy---------------------------------------------%


y_norm=y_down/max(abs(y_down));
noise=-(y_norm.^2).*log(y_norm.^2);
P=0.02*fd;
k=1;
i=1;
noise_average=zeros(1,length(y_norm));

while i<length(y_norm)  
    for i=i-P:i
        if i<=0
            yy = 0;
        else 
            yy = y_norm(i);
        end
        noise_average(k)=noise_average(k)+yy.^2.*log((yy+eps(1)).^2);
    end
    k=k+1;
    i=i+1;
end
noise_average=(-1/P)*noise_average;
noise_average_prenormalized=(noise_average-mean(noise_average))/(std(noise_average));

%Offset à 0
noise_average_prenormalized = noise_average_prenormalized - min(noise_average_prenormalized); 

%Normalisation
noise_average_normalized = noise_average_prenormalized/max(abs(noise_average_prenormalized)); 

%Plot
figure(4);
td = ts  / ovS;
t_down=[0:length(y_down)-1].*td;
plot(t_down,noise_average_normalized);
title('Denoising with Shannon Energy');

%Traitement du graphique obtenu
    list_Max = zeros(length(y_norm),1);     
    boolean monte;
    monte = false; 

%% Peak Detection --------------------------------------------------------%
    
%Plot dans le domaine temporel-------------------------------------------%
data_shannon = noise_average_normalized;

valeur_max=max(data_shannon);
n=1;
p=1;
k=0;
fs = 8000;
s1=zeros(1,1);
s2=zeros(1,1);

seuil_S1=(2/3)*valeur_max;
seuil_S2=(1/50)*valeur_max;

%Traitement du graphique shannon obtenu

list_Max = zeros(length(y_norm),1); 
monte = 0; 

%Analyse des pics par Translation d un intervalle de 20 valeurs

for i = 2: length(data_shannon) - 21

    test = 0;
    monteprec = monte;
    cpt = 0;

    for j = 1:20
        if (data_shannon(i+j-1) < data_shannon(i+j))
            cpt = cpt+1;
        end
    end

    if (cpt>15)
        monte=1; 
    else 
        monte = 0;
    end


    if ((monteprec == 1) && (monte == 0))
        list_Max(i)= data_shannon(i);
    end
end

seuil = 0.015;

% ############Temps physique de cycles############## %
%
%   Durée d'une systole (S1) : 100-160 ms
%   Durée d'un diastole (S2 + S3 + S4 ...) : 150-280 ms
%   Intervalle de durées S1_a -> S1_b : 250-440 ms
%
% ################################################### %

min_systole = 0.1*fs;    %Temps MIN d'un cycle systole
max_systole = 0.16*fs;   %Temps MAX d'un cycle systole
D_systole = max_systole - min_systole;

min_diastole = 0.15*fs;  %Temps MIN d'un cycle diastole
max_diastole = 0.28*fs;  %Temps MAX d'un cycle diastole

min_S1_S1 = 0.25*fs;    %Temps MIN d'une période entre 2 S1
max_S1_S1= 0.44*fs;   %Temps MAX d'une période entre 2 S1


%min_S = min_s1 - min_s2;

k=0;

  for j = 1 : length(list_Max)-1
      if list_Max(j) < seuil
          list_Max(j)=0;
      end   
  end
  figure(5);
  plot(t_down,list_Max);
  title('list Max 1');

  % Tri des doublons
  
  max_interval = 0;
  periode = (bpm/60)*fs;
  for j = 1 : length(list_Max)-periode -1
      if list_Max(j)> 0  
        k=j+1;
        for i=k:k+(periode)
            if list_Max(i) > max_interval
                max_interval  = list_Max(i);
                %Mettre tous les list_Max(i) precedents à 0%
                for l = i-k : i-1
                    list_Max(l) = 0;
                end
            end  
        end
      end
      max_interval = 0;
  end
  
 
  figure(6);
  plot(t_down,list_Max);
  title('list Max 2');
 
  i=1;
  list_newMax(i) = 0;
  
  
  for k = 1 : length(list_Max)
      if list_Max(k) > 0
          list_newMax(i)= list_Max(k);
          i=i+1;
      end
  end
  

disp(list_newMax);