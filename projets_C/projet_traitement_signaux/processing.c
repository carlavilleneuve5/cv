// Processing functions
#include "processing.h"
#include "data_file.h"
#include "kissFFT/kiss_fft.h"
#include <stdio.h>
#include <unistd.h>
#include <stdio.h>

float get_bpm();

void benchmark(){
    int mc_runs = 50;
    struct_timer timer;
    /*--------------------------------------------------------------------------
     * --- Main benchmark call (Monte carlo)
     * --------------------------------------------------------------------------*/
    printf("Starting routine\n");
    timer = tic();
    float ppm_mean = 0;
    int ppm_tmp;
    for (int c = 0 ; c < mc_runs ; c++){
        ppm_tmp = processing();
        ppm_mean += (float) ppm_tmp;
    }
    printf("output\n");
    timer = toc(timer);
    print_toc(timer);
    float median_time = timer.duration / (float) mc_runs;
    float median_ppm  = ppm_mean / (float) mc_runs; 
    printf("The medium time per iteration is %2.4f \n",median_time);
    printf("Calculated PPM is  %2.4f \n",median_ppm);
}


int processing()
{
	float valeur;
	valeur=get_bpm();
	return (int) valeur;
}

float max(float*liste)
{

	int n=SIZE;
    int val_max = 0;
    int i=0;
    double temp = 0;
    for (i = 0; i < n; i++){
        if (temp < liste[i]){
            // We have a new max 
            temp = liste[i];
            val_max = i;
        }
    }
   return val_max;
}

float get_bpm(){
//Function get_bpm return the bpm of a file .wav of a pgc

//Loading a phonocardiogram-----------------------------------------------%

	double min_s1=0.25*Fs; //Calcul de la durée minimum d'un cycle systole/diastole (s1, s2, s3, s4)%

//Plot dans le domaine temporel-------------------------------------------%
	int j=0;
	int k=0;
	int max_seuil=20;
	int n=0;
	int cpt=0;
	int seuil;
	int N;
	int bpm;
	int valeur_max;
	float somme;
	float moyenne=0;
	double liste[max_seuil];
	N=SIZE;
	valeur_max=max(sig);
	seuil=(2/3)*valeur_max;

	for (j=0;j<N;j++){
		if (sig[j] > seuil){
			if (j > k+min_s1){
				cpt+=1;
				liste[n]=j;
				n=n+1;
				k=j;
			}
		}
	}
	for (j=1;j<cpt;j++){
		somme+=liste[j]-liste[j-1];
	}
	moyenne=somme/(n);
	bpm=1/(moyenne*1/Fs)*60;
	return bpm;
}
