/**
 *  canvascii.c
 *
 *  TP1 du module INF3135: Construction et maintenance de logiciel
 *
 *  Ce programme permet de dessiner sur un canvas ASCII.
 *
 *  @ author: Carla Villeneuve
 *  @ version: 06/02/2023
 *  @ code permanent : VILC67270107
 */

// Inclusion des bibliothèques

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

// Definitions de constantes

#define MAX_HEIGHT 40
#define MAX_WIDTH 80
#define USAGE "\
Usage: ./canvascii [-n HEIGHT,WIDTH] [-s] [-k] [-p CHAR]\n\
          [-h ROW] [-v COL] [-r ROW,COL,HEIGHT,WIDTH]\n\
          [-l ROW1,COL1,ROW2,COL2] [-c ROW,COL,RADIUS]\n\
Draws on an ASCII canvas. The canvas is provided on stdin and\n\
the result is printed on stdout. The dimensions of the canvas\n\
are limited to at most 40 rows and at most 80 columns.\n\
\n\
If no argument is provided, the program prints this help and exit.\n\
\n\
Canvas options:\n\
  -n HEIGHT,WIDTH           Creates a new empty canvas of HEIGHT rows and\n\
                            WIDTH columns. Should be used as first option,\n\
                            otherwise, the behavior is undefined.\n\
                            Ignores stdin.\n\
  -s                        Shows the canvas and exit.\n\
  -k                        Enables colored output. Replaces characters\n\
                            between 0 and 9 by their corresponding ANSI\n\
                            colors:\n\
                              0: black  1: red      2: green  3: yellow\n\
                              4: blue   5: magenta  6: cyan   7: white\n\
\n\
Drawing options:\n\
  -p CHAR                   Sets the pen to CHAR. Allowed pens are\n\
                            0, 1, 2, 3, 4, 5, 6 or 7. Default pen\n\
                            is 7.\n\
  -h ROW                    Draws an horizontal line on row ROW.\n\
  -v COL                    Draws a vertical line on column COL.\n\
  -r ROW,COL,HEIGHT,WIDTH   Draws a rectangle of dimension HEIGHTxWIDTH\n\
                            with top left corner at (ROW,COL).\n\
  -l ROW1,COL1,ROW2,COL2    Draws a discrete segment from (ROW1,COL1) to\n\
                            (ROW2,COL2) with Bresenham's algorithm.\n\
  -c ROW,COL,RADIUS         Draws a circle centered at (ROW,COL) of\n\
                            radius RADIUS with the midpoint algorithm.\n\
"

//Couleurs de fond
//
#define NOIR    "\e[40m"
#define ROUGE   "\e[41m"
#define VERT    "\e[42m"
#define JAUNE   "\e[43m"
#define BLEU    "\e[44m"
#define MAGENTA "\e[45m"
#define CYAN    "\e[46m"
#define BLANC   "\e[47m"
#define reset   "\e[0m"

// Déclaration des types

typedef struct canvas {
    char pixels[MAX_HEIGHT][MAX_WIDTH]; // A matrix of pixels
    unsigned int width;                 // Its width
    unsigned int height;                // Its height
    char pen;                           // The character we are drawing with
}Canvas;

enum error {
    OK                         = 0, // Everything is ok
    ERR_WRONG_PIXEL            = 1, // Wrong pixel value in canvas
    ERR_CANVAS_TOO_HIGH        = 2, // Canvas is too high
    ERR_CANVAS_TOO_WIDE        = 3, // Canvas is too wide
    ERR_CANVAS_NON_RECTANGULAR = 4, // Canvas is non rectangular
    ERR_UNRECOGNIZED_OPTION    = 5, // Unrecognized option
    ERR_MISSING_VALUE          = 6, // Option with missing value
    ERR_WITH_VALUE             = 7  // Problem with value
};

// Déclaration des fonctions

/**
 * Indique si le pixel contient une valeur valide
 *
 * @param caractere    Le caractere à tester
 * @return             Booléen qui renvoie true si le caractère est valide
 *                     et false le cas échéant
 */

bool est_caractere_valide(char caractere);

/**
 * Créer un canvas vide de taille choisie
 *
 * @param height   La hauteur du canvas que l'on souhaite créer
 * @param width    La largeur du canvas que l'on souhaite créer
 * @return         Canvas vide de hauteur et de largeur désigné.
 *
 */

Canvas create_empty_canvas(int nb_lines, int nb_columns);

/**
 * Affiche un canvas sur stdout
 *
 * @param canvas   Le canvas que l'on souhaite afficher sur stdout
 */

void afficher_canvas(Canvas canvas);

/**
 * Afficher un canvas en couleur sur stdout
 *
 * @param canvas Le canvas que l'on souhaite afficher sur stdout
 */

void afficher_canvas_couleur(Canvas canvas);

/**
 * Affiche le message d'erreur sur stderr
 *
 * @param error          L'erreur à déclarer
 * @param source_erreur  Le caractère ou l'option à l'origine de l'erreur
 */

void afficher_erreur(enum error value, char source_erreur);

/** 
 * Lire un canvas sur stdin
 *
 * @return     Le Canvas lue sur stdin
 */

Canvas read_canvas();

/**
 * Tracer un segment d'apres l'algorithme de Bresenham
 *
 * @param x0 Le premier point de coordonnée en x
 * @param y0 Le premier point de coordonnée en y
 * @param x1 Le deuxième point de coordonnée en x
 * @param y1 Le deuxième point de coordonnée en y
 * @return   Le canvas modifié
 */

Canvas plot_line(Canvas canvas, int x0, int y0, int x1, int y1);

/**
 * Tracer un cercle a partir de l'algorithme de tracé d'arc de cercle
 * par point milieu. Code recupéré sur le site Rosetta Code.
 *
 * @param x0      Point de centre du cercle en x
 * @param y0      Point de centre du cercle en y
 * @param radius  Rayon du cercle 
 */

Canvas tracer_cercle(Canvas canvas, int x0, int y0, int radius);

// Implémentation des fonctions

bool est_caractere_valide(char caractere){
    bool result=false;
    if ((caractere >= '0' && caractere <= '7') || caractere == '.'){
        result=true;
    }
    return result;
}


Canvas create_empty_canvas(int height, int width){
    Canvas canvas;
    canvas.height=height;
    canvas.width=width;
    for (unsigned int i = 0;i < canvas.height; i++){
        for(unsigned int j = 0;j < canvas.width; j++){
            canvas.pixels[i][j]='.';
        }
    }
    return canvas;
}


void afficher_canvas(Canvas canvas){
    for (unsigned int i = 0;i < canvas.height; i++){
        for (unsigned int j = 0;j < canvas.width; j++){
            printf("%c",canvas.pixels[i][j]);
        }
        printf("\n");
    }
}

void afficher_canvas_couleur(Canvas canvas){
    for (unsigned int i = 0; i < canvas.height; i++){
        for (unsigned int j = 0; j < canvas.width; j++){
            switch(canvas.pixels[i][j]){
                case '.':
                    printf(" ");
                break;
                case '0':
                    printf(NOIR " " reset);
                break;
                case '1':
                    printf(ROUGE " " reset);
                break; 
                case '2':
                    printf(VERT " " reset);
                break;
                case '3':
                    printf(JAUNE " " reset);
                break;
                case '4':
                    printf(BLEU " " reset);
                break;
                case '5':
                    printf(MAGENTA " " reset);
                break;
                case '6':
                    printf(CYAN " " reset);
                break;
                case '7':
                    printf(BLANC " " reset);
                break;
            }
        }
        printf("\n");
    }
}


Canvas read_canvas(){
    Canvas canvas;
    char ligne[MAX_WIDTH + 1];
    int i=0;
    int return_value;
    // On regarde la premiere ligne sur stdin 
    return_value = fscanf(stdin,"%s",ligne);
    if (ligne[0] == '\0'){
        afficher_erreur(ERR_MISSING_VALUE,'\0'); 
        exit(ERR_MISSING_VALUE);
    } else {
        if (strlen(ligne) > MAX_WIDTH){
            afficher_erreur(ERR_CANVAS_TOO_WIDE,'\0');
            exit(ERR_CANVAS_TOO_WIDE);
        }
        canvas.width = strlen(ligne);
        while(return_value > 0){
            if(strlen(ligne) != canvas.width){
                afficher_erreur(ERR_CANVAS_NON_RECTANGULAR,'\0');
                exit(ERR_CANVAS_NON_RECTANGULAR);
            }
            for (unsigned int j = 0; j < (unsigned int) strlen(ligne) ; j++){
            // On insere les pixels de la ligne dans le tableau
                if (!est_caractere_valide(ligne[j])){
                    afficher_erreur(ERR_WRONG_PIXEL,ligne[j]);
                    exit(ERR_WRONG_PIXEL);
                }   
                canvas.pixels[i][j]=ligne[j];
            }
            i++;
            // On regarde la prochaine ligne
            return_value = fscanf(stdin,"%s",ligne);
        }
        if( i > MAX_HEIGHT){
            afficher_erreur(ERR_CANVAS_TOO_HIGH,'\0');
            exit(ERR_CANVAS_TOO_HIGH);
        }
        canvas.height = i;
    }
    return canvas;
}

void afficher_erreur(enum error value, char source_erreur){
    switch(value){
        case 0:
            // Rien à afficher
        break;
        case 1:
            fprintf(stderr,"Error: wrong pixel value %c\n%s",source_erreur,USAGE);       
        break;
        case 2:
            fprintf(stderr,"Error: canvas is too high (max height: 40)\n%s",USAGE);
        break;
        case 3:
            fprintf(stderr,"Error: canvas is too wide (max width: 80)\n%s",USAGE);
        break;
        case 4:
            fprintf(stderr,"Error: canvas should be rectangular\n%s",USAGE);
        break;
        case 5:
            fprintf(stderr,"Error: unrecognized option -%c\n%s",source_erreur,USAGE);
        break;
        case 6:
            fprintf(stderr,"Error: missing value with option -%c\n%s",source_erreur,USAGE);
        break;
        case 7:
            fprintf(stderr,"Error: incorrect value with option -%c\n%s",source_erreur,USAGE);
        break;
    }
}

Canvas plot_line(Canvas canvas, int x0, int y0, int x1, int y1){
    int dx,dy;
    int sx,sy;
    int error;
    int e2;
    dx = abs(x1 - x0);
    if (x0 < x1){
        sx = 1;
    } else {
        sx = -1;
    }
    dy = -abs(y1 - y0);
    if(y0 < y1){
        sy = 1;
    } else {
        sy = -1;
    }
    error = dx + dy;
    canvas.pixels[x0][y0] = canvas.pen;

    while(!(x0 == x1 && y0 == y1)){
        e2 = 2 * error;
        if(e2 >= dy){
            if (x0 != x1){
                error = error + dy;
                x0 = x0 + sx;
            }
        }
        if (e2 <= dx){
            if (y0 != y1){
                error = error + dx;
                y0 = y0 + sy;
            }
        }
        canvas.pixels[x0][y0] = canvas.pen;
    }
    return canvas;
}

Canvas tracer_cercle(Canvas canvas, int x0, int y0, int rayon){
    int f;
    int ddF_x;
    int ddF_y;
    int x,y;

    f = 1 - rayon;
    ddF_x = 0;
    ddF_y = -2 * rayon;
    x = 0;
    y = rayon;
    canvas.pixels[x0][y0 + rayon] = canvas.pen;
    canvas.pixels[x0][y0 - rayon] = canvas.pen;
    canvas.pixels[x0 + rayon][y0] = canvas.pen;
    canvas.pixels[x0 - rayon][y0] = canvas.pen;
    while (x < y){
        if (f >= 0){
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x + 1;
        canvas.pixels[x0 + x][y0 + y] = canvas.pen;
        canvas.pixels[x0 - x][y0 + y] = canvas.pen;
        canvas.pixels[x0 + x][y0 - y] = canvas.pen;
        canvas.pixels[x0 - x][y0 - y] = canvas.pen;
        canvas.pixels[x0 + y][y0 + x] = canvas.pen;
        canvas.pixels[x0 - y][y0 + x] = canvas.pen;
        canvas.pixels[x0 + y][y0 - x] = canvas.pen;
        canvas.pixels[x0 - y][y0 - x] = canvas.pen;
    }
    return canvas;
}


// Fonction main

int main(int argc, char *argv[]) {
    // Cas sans arguments
    if (argc==1){
        printf("%s",USAGE);
    } else {
        Canvas canvas;
        bool affichage_couleur = false;
        // Si l'option de création n'est pas écrite, on lit le canvas sur stdin
        if(strcmp(argv[1],"-n") != 0){
            canvas = read_canvas();
            canvas.pen = '7';
        }
        for(int a = 1; a < argc; a += 2){
            // Cas de l'option -n
            if (strcmp(argv[a],"-n")==0){
                char *token;
                unsigned int nb_lines,nb_columns;
                if (argc < 3){ 
                    afficher_erreur(ERR_MISSING_VALUE,argv[a][1]);
                    exit(ERR_MISSING_VALUE);
                }
                // Extraction du premier nombre de l'argument
                token=strtok(argv[a+1],",");
                if (token != NULL){
                    //Conversion du nombre en entier
                    nb_lines=atoi(token);
                    if (nb_lines<=0 || nb_lines > MAX_HEIGHT){
                        afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                        exit(ERR_WITH_VALUE);
                    }
                } else{
                    afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                    exit(ERR_WITH_VALUE);
                }
                //Extraction du deuxieme nombre en argument
                token=strtok(NULL,",");
                if (token !=NULL){
                    // Conversion du nombre en entier
                    nb_columns=atoi(token);
                    if (nb_columns<=0 || nb_columns > MAX_WIDTH){
                        afficher_erreur(ERR_WITH_VALUE,argv[a][1]); 
                        exit(ERR_WITH_VALUE);
                    }
                } else{
                    afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                    exit(ERR_WITH_VALUE);
                }
                // Creation d'un canvas vide de taille indiqué et affichage
                canvas=create_empty_canvas(nb_lines,nb_columns);
                canvas.pen = '7';
            // Cas de l'option -s
            } else if (strcmp(argv[a],"-s")==0){
                // L'option est reconnue et on affiche le canvas à la fin
            // Cas de l'option -h
            } else if (strcmp(argv[a],"-h") == 0){
                int line_value;
                if (argc == a + 1){
                    afficher_erreur(ERR_MISSING_VALUE,argv[a][1]);
                    exit(ERR_MISSING_VALUE);
                }
                line_value = atoi(argv[a+1]);
                if ((line_value == 0 && strcmp(argv[a+1],"0") != 0)
                         || line_value < 0 || (unsigned int) line_value >= canvas.height){
                    afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                    exit(ERR_WITH_VALUE);
                } else {
                    for (unsigned int i = 0; i < canvas.width; i++){
                        canvas.pixels[line_value][i] = canvas.pen;
                    }
                }
            // Cas de l'option -v
            } else if (strcmp(argv[a],"-v") == 0){
                int column_value;
                if (argc == a + 1){
                    afficher_erreur(ERR_MISSING_VALUE,argv[a][1]);
                    exit(ERR_MISSING_VALUE);
                }
                column_value = atoi(argv[a+1]);
                if ((column_value == 0 && strcmp(argv[a+1],"0") != 0) 
                         || column_value < 0 || (unsigned int) column_value >= canvas.width){
                    afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                    exit(ERR_WITH_VALUE);
                } else{
                    for (unsigned long i = 0; i < canvas.height; i++){
                        canvas.pixels[i][column_value] = canvas.pen;
                    }
                }
            // Cas de l'option -r
            } else if (strcmp(argv[a],"-r") == 0){
                char* token;
                int x,y;
                int largeur;
                int hauteur;
                token = strtok(argv[a+1],",");
                if (token != NULL){
                    x = atoi(token);
                    token = strtok(NULL,",");
                    y = atoi(token);
                    token = strtok(NULL,",");
                    hauteur = atoi(token);
                    token = strtok(NULL,",");
                    largeur = atoi(token);
                    if (largeur <= 0 || hauteur <= 0){
                        afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                        exit(ERR_WITH_VALUE);
                    }
                } else {
                    afficher_erreur(ERR_MISSING_VALUE,argv[a][1]);
                    exit(ERR_MISSING_VALUE);
                }
                for (int i = y; i < largeur + y ; i++){
                    if ( i >= 0 && i < (int) canvas.width){
                         canvas.pixels[x][i] = canvas.pen; 
                         if ( x + hauteur - 1 < (int) canvas.height){
                            canvas.pixels[x + hauteur - 1][i] = canvas.pen;
                         }
                    }
                }
                for (int i = x; i < hauteur + x ; i++){ 
                    if (i >= 0 && i < (int) canvas.height){;
                        canvas.pixels[i][y] = canvas.pen;
                        affichage_couleur = false;//Chercher pourquoi la ligne d'avant change la valeur du bool
                        if ( y + largeur - 1 < (int) canvas.width){
                            canvas.pixels[i][y + largeur -1] = canvas.pen;
                        }
                    }
                }
            // Cas de l'option -p
            } else if (strcmp(argv[a],"-p") == 0){
                if (est_caractere_valide(argv[a+1][0]) && strlen(argv[a+1]) == 1){
                    canvas.pen = argv[a+1][0];
                } else {
                    afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                    exit(ERR_WITH_VALUE);
                }
            // Cas de l'option -c
            } else if (strcmp(argv[a],"-c") == 0){
                char* token;
                int x,y;
                int rayon;
                token = strtok(argv[a + 1],",");
                if (token != NULL){
                    x = atoi(token);
                    token = strtok(NULL,",");
                    y = atoi(token);
                    token = strtok(NULL,",");
                    rayon = atoi(token);
                    if (rayon <= 0){
                        afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                        exit(ERR_WITH_VALUE);
                    }
                } else {
                    afficher_erreur(ERR_MISSING_VALUE, argv[a][1]);
                    exit(ERR_MISSING_VALUE);
                }
                canvas = tracer_cercle(canvas,x,y,rayon);
            // Cas de l'option -k
            } else if (strcmp(argv[a],"-k") == 0){
                affichage_couleur = true;
                // Comme l'option ne prend pas de parametre, on regarde l'argument suivant
                a -=1; 
            // Cas de l'option -l
            } else if (strcmp(argv[a],"-l") == 0){
                char *token;
                int x0,y0;
                int x1,y1;
                token = strtok(argv[a + 1],",");
                if (token != NULL){
                    x0 = atoi(token);
                    token = strtok(NULL,",");
                    y0 = atoi(token);
                    token = strtok(NULL,",");
                    x1 = atoi(token);
                    token = strtok(NULL,",");
                    y1 = atoi(token);
                } else {
                    afficher_erreur(ERR_WITH_VALUE,argv[a][1]);
                    exit(ERR_MISSING_VALUE);
                }
                canvas = plot_line(canvas,x0,y0,x1,y1);
            } else {
                afficher_erreur(ERR_UNRECOGNIZED_OPTION,argv[a][1]);
                exit(ERR_UNRECOGNIZED_OPTION);
            }
        }
        // Affichage du canvas
        if (affichage_couleur){
            afficher_canvas_couleur(canvas);
        } else {
            afficher_canvas(canvas);
        }
    }
    return OK;
}

