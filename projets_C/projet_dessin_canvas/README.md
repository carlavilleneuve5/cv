
# TP1: Dessiner sur un canevas ASCII"

## Description

Le programme C nommée `canvascii.c` permet de dessiner sur un canevas ASCII.
Ce travail est réalisée dans le cadre du TP1 du cours de Construction et 
Maintenance de logiciels (INF3135) qui est un cours du Baccalauréat de 
systèmes informatiques et éléctroniques de l'UQAM, enseignée par Mr Serge Dogny.

Avant de dessiner sur un canevas, on peut
* soit afficher le manuel d'utilisation du programme en précisant
    aucun argument
* soit lire un canevas sur l'entrée 
  `stdin` en redirigeant un fichier .canvas sur l'entrée standard, 
* soit créer un canevas avec l'option `-n` de dimensions souhaitées.

Pour dessiner des formes sur le canevas, plusieurs options sont disponibles:

* `-h` : dessiner une ligne horizontale sur le canevas
* `-v` : dessiner une ligne verticale sur le canevas
* `-r` : dessiner un rectangle sur le canevas
* `-l` : dessiner un segment sur le canevas
* `-c` : dessiner un cercle sur le canevas


Enfin, on possède 2 options supplémentaires:

* `-p` : permet de changer la valeur du stylo avec lequel on dessine, 
  on doit saisir une valeur comprise entre 0 et 7 inclus, sinon une erreur
  est génerée. 
* `-k` : permet d'afficher le canevas en couleur.

Ci joint le lien vers le [sujet du travail](sujet.md).

## Auteur

Carla Villeneuve (VILC67270107)

## Fonctionnement

Pour faire fonctionner ce projet, vous devez ouvrir un terminal et vous placez dans
le dossier de ce projet. Ensuite, pour compiler ce projet, un Makefile a déjà été 
configuré, vous devez alors saisir la commande suivante:

```sh
$ make
```

Ensuite, pour exécuter ce programme vous devrez saisir la commande `./canvascii` suivi
de(s) option(s) souhaitée(s). Vous pouvez trouver ci-joint des exemples d'utilisation
des options. Toutes les options sont combinables. En revanche, si l'option `-n` est saisie et 
qu'un canevas est placée sur stdin, il sera ignoré. 


Si on souhaite créer un canevas, on utilise l'option `-n`, suivi d'une paire de 2 
dimensions (hauteur et largeur) inférieur à 40 pour la hauteur et à 80 pour
la largeur.
Example:

```sh
$ ./canvascii -n 3,5
.....
.....
.....
```

Si on souhaite seulement afficher un canevas depuis `stdin`, on utilise 
l'option `-s`.

Example:

```sh
$ ./canvascii -s < examples/empty5x8.canvas
........
........
........
........
........
```

Pour les options `-h`et `-v`, on précise un numéro de ligne sur laquelle dessiner. 
Si le nombre rensigné est extérieur à la taille du canevas, on a une erreur.

Example:

```sh
$ ./canvascii -n 3,3 -h 1
...
777
...
```

Pour l'option `-r`, on précise 4 nombres:
* la coordonnée x du point de départ du rectangle
* la coordonnée y du point de départ du rectangle
* la largeur du rectangle
* la hauteur du rectangle.

Les coordonnées du point de départ peuvent être des valeurs négatives mais
les dimensions de hauteur et largeur doivent être positives, sinon on a une
erreur. 
Si le tracé du rectangle sort de la zone du canvevas, on ne trace que ce qui 
rentre dans la taille du canevas.

Example:

```sh
$ ./canvascii -n 5,5 -r 1,1,3,3
.....
.777.
.7.7.
.777.
```

Pour l'option `-l`, on précise 4 nombres:
* la coordonnée x du point A
* la coordonnée y du point A
* la coordonnée x du point B
* la coordonnée y du point B

Les coordonnées des points peuvent être des valeurs négatives. 
Si le tracé du segment sort de la zone du canvevas, on ne trace que ce qui 
rentre dans la taille du canevas.

Example:
```sh
$ ./canvascii -n 5,5 -l 1,1,4,4
.....
.7...
..7..
...7.
.....
```

Pour l'option `-c`, on précise 3 nombres:
* la coordonnée x du centre du cercle
* la coordonnée y du centre du cercle
* le rayon du cercle

Example:
```sh
$ ./canvascii -n 5,5 -c 2,2,2
.777.
7...7
7...7
7...7
.777.
```

Les coordonnées du point peuvent être des valeurs négatives mais la valeur
du rayon doit être positive, sinon une erreur est générée. 
Si le tracé du cercle sort de la zone du canvevas, on ne trace que ce qui 
rentre dans la taille du canevas.

## Gestion erreur

7 codes de retour peuvent être obtenus:

* `0` : Aucune erreur n'est survenu
* `1` : Valeur de pixel eronnée dans le canvas
* `2` : Canevas trop haut
* `3` : Canevas trop large
* `4` : Canevas non rectangulaire
* `5` : Option non reconnue
* `6` : Valeur manquante avec l'option
* `7` : Valeur rensignée éronnée

## Tests

Pour lancer la liste de tests, un Makefile a été configuré. Il suffit alors de 
lancer la commande suivante:

```sh
$ make test
```

La série de test automatique se lance et le résultat obtenue est:

34 tests, 0 failure


## Dépendances

Les dépendances du projets sont 
* le compilateur GCC
* [Bats](https://github.com/bats-core/bats-core) pour la réalisation des tests
* pandoc pour le transfert des Markdown en .html

## Références

Pour compléter ce projet, les références utilisés sont:

* [l'algorithme de tracé d'arc de cercle par point milieu](https://rosettacode.org/wiki/Bitmap/Midpoint_circle_algorithm#C) du site Rosetta Code,
    afin de réaliser l'algorithme de tracé du cercle
* [l'algorithme de Bresenham](https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm) sur la page Wikipedia, afin de réaliser le tracé d'un
    segment.

